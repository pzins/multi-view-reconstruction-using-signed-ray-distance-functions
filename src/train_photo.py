import argparse
import os

import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger

from PhotoDataset import *
from PhotoNet import *
from utils import *

# np.random.seed(10)
# random.seed(10)

parser = argparse.ArgumentParser(description='Learn Photoconsistency')
parser.add_argument('-d', '--dataset', action='store', required=True, type=str, help='Dataset')
parser.add_argument('-s', '--samples', action='store', default="rays_samples_gt", type=str, help='Ray samples folder')
parser.add_argument('-n', '--name', action='store', default="dbg", type=str, help='Experiment name')
parser.add_argument('-o', '--out_path', action='store', default="logs", type=str, help='Output and logs folder')
parser.add_argument('--depthmaps_gt', action='store', default="depthmaps_gt", type=str, help='Ground Truth depthmaps folder')
parser.add_argument('--mesh_gt', action='store', default="mesh_gt", type=str, help='Ground Truth mesh folder')
parser.add_argument('--load_checkpoint', action='store', type=str, help='Load checkpoint')
parser.add_argument('--no_print', action='store_true', help='Disable progress bar')
parser.add_argument('--num_epoch', action='store', type=int, default=1000, help='Number of epochs')
parser.add_argument('--freq_val', action='store', type=int, default=1, help='Frequency of validation')
parser.add_argument('--freq_log', action='store', type=int, default=50, help='Frequency of logs')
parser.add_argument('--num_rays', action='store', type=int, default=1000, help='Number of rays at each iteration')
parser.add_argument('--num_samples', action='store', type=int, default=51, help='Per ray number of samples')
parser.add_argument('-ol', '--offset_low', action='store', default=3, type=float, help='Samples offset range lower bound')
parser.add_argument('-oh', '--offset_high', action='store', default=25, type=float, help='Samples offset range higher bound')
parser.add_argument('--lr', action='store', type=float, default=0.0001, help='Learning rate')
parser.add_argument('--num_workers', action='store', type=int, default=4, help='Dataloader: number of workers')
parser.add_argument('--max_subjects_tr', action='store', type=int, default=-1, help='Maximum number of subjects during training')
parser.add_argument('--max_subjects_val', action='store', type=int, default=-1, help='Maximum number of subjects during validation')
parser.add_argument('--prefetch', action='store', type=int, default=2, help='Prefetch factor')
parser.add_argument('--rescale', action='store', default=1, type=float, help='Rescale')
parser.add_argument('--window', action='store', type=int, default=0, help='window of neighbors (odd number)')
parser.add_argument('-dt', '--data_type', action='store', default="RP", type=str, help='[RP, DTU, Kinovis]')
args = parser.parse_args()

random_lights = False
random_lights = True


# All the scans minus [15 test scans + scans showing a test object + scan054 (only white)]
with open("./configs/train_dtu.txt") as f:
    subjects_tr = f.read().splitlines()

with open("./configs/test_dtu.txt") as f:
    subjects_val = f.read().splitlines()

   


if args.max_subjects_tr > 0:
    subjects_tr = subjects_tr[:args.max_subjects_tr]
if args.max_subjects_val > 0:
    subjects_val = subjects_val[:args.max_subjects_val]

depthmaps_gt_path = os.path.join(args.dataset, args.depthmaps_gt)
mesh_gt_path = os.path.join(args.dataset, args.mesh_gt)


data_cfg = {
    "dataset": args.dataset,
    "DEPTH_NO_SURFACE": DEPTH_NO_SURFACE,
    "samples_path": os.path.join(args.dataset, args.samples),
    "subjects_tr": subjects_tr,
    "subjects_val": subjects_val,
    "num_workers": args.num_workers,
    "num_rays": args.num_rays,
    "per_ray_samples": args.num_samples,
    "offset_low": args.offset_low,
    "offset_high": args.offset_high,
    "prefetch": args.prefetch,
    "gt_depthmaps_path": depthmaps_gt_path,
    "gt_mesh_path": mesh_gt_path,
    "data_type": args.data_type,
    "rescale": args.rescale,
    "window": args.window,
    "random_lights": random_lights,
}

data_module = PhotoDataModule(data_cfg)


model_cfg = {
    "num_rays": args.num_rays,
    "lr": args.lr,
    "data_type": args.data_type,
    "window": args.window,
}

model = PhotoNet(cfg=model_cfg)

if args.rescale == 1:
    model.pool_1 = True
    model.pool_2 = True
elif args.rescale == 0.5:
    # if train DTU low res, remove pooling
    model.pool_1 = False
    model.pool_2 = False
else:
    print("Unkown rescale")
    exit(0)


logger = TensorBoardLogger(args.out_path, args.name)
cb_acc_val = ModelCheckpoint(filename="{epoch}-{step}" + "-acc_val", monitor='Accuracy/val', save_top_k=5, mode='max')
cb_loss_tr = ModelCheckpoint(filename="{epoch}-{step}" + "-loss_train", monitor='Loss/train', save_top_k=5, mode='min')
cb_loss_val = ModelCheckpoint(filename="{epoch}-{step}" + "-loss_val", monitor='Loss/val', save_top_k=5, mode='min')
latest = ModelCheckpoint(filename="{epoch}-{step}" + "-latest", every_n_epochs=2, save_last=True)

trainer = pl.Trainer(
    enable_progress_bar= not args.no_print,
    callbacks=[cb_loss_tr, cb_loss_val, cb_acc_val, latest],
    strategy='dp',
    num_sanity_val_steps=0,
    logger=logger,
    max_epochs=args.num_epoch,
    check_val_every_n_epoch=args.freq_val,
    log_every_n_steps=args.freq_log,
    gpus=1
)
trainer.fit(model, datamodule=data_module, ckpt_path=args.load_checkpoint)