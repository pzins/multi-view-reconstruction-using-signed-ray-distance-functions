import argparse
import os

import numpy as np
import torch
from scipy.io import loadmat
from skimage import measure

from utils import *

# Parameters
# ------------------------------------------------------------------------------------------------
dtu_base_path = "/disk/data/DTU/"
# ------------------------------------------------------------------------------------------------


parser = argparse.ArgumentParser(description='Visual Hull')
parser.add_argument('-d', '--dataset', action='store', required=True, type=str, help='Dataset folder')
parser.add_argument('-s', '--subject', action='store', required=True, type=str, help='Subject')
parser.add_argument('--rescale', action='store', default=1, type=float, help='Rescale')
parser.add_argument('-dt', '--data_type', action='store', default="RP", type=str, help='[RP, DTU, Kinovis]')
parser.add_argument('-o', '--out', action='store', default="mesh_vh", type=str, help='Output folder')
args = parser.parse_args()


# Cameras
if args.data_type == "DTU":
    if int(args.subject[4:]) < 82:
        cams = list(range(1,50))
    else:
        cams = list(range(1,65))
elif args.data_type == "RP":
    cams = list(range(270,360,10)) + list(range(0,90,10))
elif args.data_type == "Kinovis":
    cams = list(range(0,65))
elif args.data_type == "BlendedMVS":
    res = get_cameras_cluster(args.dataset, args.subject)
    cams = list(res.keys())


mesh_name = "mesh.ply"
if args.data_type == "DTU":
    mesh_name = "poisson_" + args.subject[4:] + "_trimmed.ply"
elif args.data_type == "RP":
    mesh_name = args.subject + "_100k.obj"
elif args.data_type == "Kinovis":
    mesh_name = args.subject + ".obj"
elif args.data_type == "BlendedMVS":
    mesh_name = "mesh.obj"


out_folder = os.path.join(args.dataset, args.out, args.subject)
os.makedirs(out_folder, exist_ok=True)
out_file = os.path.join(out_folder, mesh_name)

use_cuda = False
use_cuda = True
device = torch.device("cuda" if use_cuda else "cpu")


bb_path = None
if args.data_type == "DTU":
    bb_path = dtu_base_path + "SampleSet/MVS Data/ObsMask/ObsMask%d_10.mat" % int(args.subject[4:])
    bb = loadmat(bb_path)['BB']
    # out % of nb_cams over which point out of image is 0
    config = {
        "scan024": { "dil": 15, "out": 0.9},
        "scan037": { "dil": 15, "out": 1},
        "scan040": { "dil": 15, "out": 0.7},
        "scan055": { "dil": 20, "out": 0.95},
        "scan063": { "dil": 0, "out": 0.95},
        "scan065": { "dil": 20, "out": 0.95},
        "scan069": { "dil": 30, "out": 0.8},
        "scan083": { "dil": 50, "out": 0.95},
        "scan097": { "dil": 50, "out": 1},
        "scan105": { "dil": 20, "out": 0.95},
        "scan106": { "dil": 20, "out": 1},
        "scan110": { "dil": 0, "out": 0.95},
        "scan114": { "dil": 10, "out": 0.95},
        "scan118": { "dil": 10, "out": 0.9},
        "scan122": { "dil": 10, "out": 0.9},
    }
    resolution = 512
    ok_out_factor = config[args.subject]["out"]
    dilation_factor = config[args.subject]["dil"]

    bb[0] -= 10
    bb[1] += 10

    if args.subject == "scan110":
        bb[0] -= 30
    if args.subject == "scan040":
        bb[0] -= 20
    if args.subject == "scan118":
        bb[0] -= 20
    if args.subject == "scan122":
        bb[0] -= 20
    if args.subject == "scan055":
        bb[0] -= 20

elif args.data_type == "RP":
    bb = np.array([[-50, 0, -40], [50,200,100]])
    resolution = 512
    ok_out_factor = 0.5
    dilation_factor = 0

elif args.data_type == "Kinovis":
    bb = np.array([[-0.5, -0.5, 0], [0.5, 0.5, 1.9]])
    resolution = 512
    ok_out_factor = 0.9
    dilation_factor = 0

elif args.data_type == "BlendedMVS":
    bb = np.array([[-0.6, -0.3, -0.7], [0.6,0.5,0.8]])
    resolution = 512
    ok_out_factor = 0.5
    dilation_factor = 0




batch_size = 1000000

x_ = np.linspace(0, abs(bb[0,0]-bb[1,0]), resolution)
y_ = np.linspace(0, abs(bb[0,1]-bb[1,1]), resolution)
z_ = np.linspace(0, abs(bb[0,2]-bb[1,2]), resolution)
b1, b2, b3 = np.meshgrid(x_, y_, z_, indexing='ij')
grid = np.vstack((b1.flatten(), b2.flatten(), b3.flatten())).T
points = np.hstack((grid, np.ones((grid.shape[0], 1)))).astype(np.float32).T
offset = np.array(bb[0]).reshape(-1,1)

points[:3,:] += offset

points = torch.tensor(points).to(device)


# points: (4, N)
mat = np.eye(4)
mat[0,0] = abs(bb[0,0]-bb[1,0]) / resolution
mat[1,1] = abs(bb[0,1]-bb[1,1]) / resolution
mat[2,2] = abs(bb[0,2]-bb[1,2]) / resolution
mat[0:3,3] = offset.reshape(-1)


rescale = None
if args.rescale != 1:
    rescale = args.rescale
data = load_data(args.data_type, args.dataset, args.subject, cams, load_mask=True, load_mesh=False, load_image=False, rescale=rescale, dilate_mask=dilation_factor)
calib_tensor = torch.tensor(data["calib"])
proj_tensor = data["proj"]
mask_tensor = torch.tensor(data["mask"])


calib_tensor = calib_tensor.to(device)
mask_tensor = mask_tensor.to(device)
K = torch.tensor(proj_tensor).to(device)

num_batchs = points.shape[1] // batch_size + 1

voxels = torch.zeros((resolution, resolution, resolution)).reshape(-1).to(device)

for i in range(num_batchs):
    pts = points[:, i*batch_size:(i+1)*batch_size]
    ref_points = world_to_view_persp(pts, calib_tensor, K, data_type=args.data_type)
    ref_points = ref_points.permute(0,2,1)
    if args.data_type != "RP":
        ref_points[..., 0] = (ref_points[..., 0] / (mask_tensor.shape[2] / 2)) - 1
        ref_points[..., 1] = (ref_points[..., 1] / (mask_tensor.shape[1] / 2)) - 1
    res = torch.nn.functional.grid_sample(mask_tensor[:,None,:,:], ref_points[:,:,None,:2], align_corners=True, mode="bilinear")

    # Allow points outside an image
    mask = (ref_points[...,0] > 1) | (ref_points[...,0] < -1) | (ref_points[...,1] > 1) | (ref_points[...,1] < -1)
    res = res[:,0,:,0]
    res[mask] = 1
    # inside at least some images
    mask2 = mask.sum(0) > (ok_out_factor * len(cams))
    
    # clean
    res = res.prod(0).bool()

    res[mask2] = 0

    voxels[i*batch_size:(i+1)*batch_size] = res

voxels = voxels.reshape(resolution, resolution, resolution)
verts, faces, normals, values = measure.marching_cubes(voxels.cpu().numpy(), 0.5, gradient_direction='ascent')

verts = np.matmul(mat[:3, :3], verts.T) + mat[:3, 3:4]
verts = verts.T

mesh = trimesh.Trimesh(vertices=verts, faces=faces)

# Keep largest component
components = mesh.split(only_watertight=False)
areas = np.array([c.area for c in components], dtype=np.float)
mesh = components[areas.argmax()]

mesh.export(out_file)
