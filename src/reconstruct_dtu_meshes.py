import argparse
import os

from tqdm import tqdm

from utils import *

# Parameters
# ------------------------------------------------------------------------------------------------
spsr_exe_path="/scratch/pzins/dev/PoissonRecon/Bin/Linux/"
# ------------------------------------------------------------------------------------------------

parser = argparse.ArgumentParser(description='Pointcloud fusion')
parser.add_argument('-i', '--input_folder', action='store', required=True, type=str, help='Input file')
parser.add_argument('-o', '--output_folder', action='store', required=True, type=str, help='Output folder')
parser.add_argument('--no_trim', action='store_true', help='Disable Surface Trimmer')
args = parser.parse_args()

os.makedirs(args.output_folder, exist_ok=True)

subjects = list(set(range(1, 129)) - set([78, 79, 80, 81])) # all subjects

for subject in tqdm(subjects):

    input_path = os.path.join(args.input_folder, "stl%03d_total.ply" % int(subject))
    output_path = os.path.join(args.output_folder, "poisson_%03d.ply" % subject)
    
    # SPSR (mkazhdan)
    num_threads = 16
    
    depth = 11
    trim_threshold = 7
    trim_threshold = 9.5
    
    run = "%sPoissonRecon --in %s --out %s --depth %d --density --threads %d" % (spsr_exe_path, input_path, output_path, depth, num_threads)
    os.system(run)

    if not args.no_trim:
        run = "%sSurfaceTrimmer --in %s --out %s --trim %f" % (spsr_exe_path, output_path, output_path[:-4] + "_trimmed.ply", trim_threshold)
        os.system(run)


