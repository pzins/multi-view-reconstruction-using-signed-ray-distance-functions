import argparse
import os

from utils import *

parser = argparse.ArgumentParser(description='Pointcloud fusion')
parser.add_argument('-i', '--input_file', action='store', required=True, type=str, help='Input file')
parser.add_argument('-o', '--output_file', action='store', required=True, type=str, help='Output file')
parser.add_argument('--depth', action='store', type=int, default=12, help='Depth of Poisson')
parser.add_argument('--threads', action='store', type=int, default=6, help='Number of threads')
parser.add_argument('--trim', action='store', type=float, default=7, help='Trimming value')
parser.add_argument('--only_trim', action='store_true', help='Disable Surface Reconstruction')
args = parser.parse_args()


# Parameters
# ------------------------------------------------------------------------------------------------
spsr_exe_path="/scratch/pzins/dev/PoissonRecon/Bin/Linux/"
# ------------------------------------------------------------------------------------------------


if not args.only_trim:
    run = "%sPoissonRecon --in %s --out %s --depth %d --density --threads %d" % (spsr_exe_path, args.input_file, args.output_file, args.depth, args.threads)
    print(run)
    os.system(run)

run = "%sSurfaceTrimmer --in %s --out %s --trim %f" % (spsr_exe_path, args.output_file, args.output_file[:-4] + "_trimmed.ply", args.trim)
print(run)
os.system(run)


