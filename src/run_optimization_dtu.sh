#!/bin/bash
optim_script=./src/main.py
rescale=1
config=./configs/conf_dtu.yaml
num_groups=7
scan="scan083"

python $optim_script -s $scan --rescale $rescale --cam_group 1 -c $config --transfo_fp32
python $optim_script -s $scan --rescale $rescale --cam_group 2 -c $config --transfo_fp32
python $optim_script -s $scan --rescale $rescale --cam_group 3 -c $config --transfo_fp32
python $optim_script -s $scan --rescale $rescale --cam_group 4 -c $config --transfo_fp32
python $optim_script -s $scan --rescale $rescale --cam_group 5 -c $config --transfo_fp32
python $optim_script -s $scan --rescale $rescale --cam_group 6 -c $config --transfo_fp32
python $optim_script -s $scan --rescale $rescale --cam_group 7 -c $config --transfo_fp32

if [ $num_groups -eq 9 ]
then
    python $optim_script -s $scan --rescale $rescale --cam_group 8 -c $config --transfo_fp32
    python $optim_script -s $scan --rescale $rescale --cam_group 9 -c $config --transfo_fp32
fi