#!/bin/bash

# Parameters
# ------------------------------------------------------------------------------------------------
# dataset name
dataset=dataset_blendedmvs
dataset=dataset_dtu
dataset=dataset_kinovis
dataset=dataset_RP

# data type
data_type=BlendedMVS
data_type=DTU
data_type=Kinovis
data_type=RP
# ------------------------------------------------------------------------------------------------


scan=$1
it=$2
depthmaps_folder=$3
rescale=""

rm -rf data/$dataset/tsdf_fusion_data/    

python ./src/prepare_data_fusion.py -i data/$dataset/$depthmaps_folder/$scan/$it -d data/$dataset/ -dt $data_type $rescale

# mv ./data/$dataset/tsdf_fusion_data/ ./data/$dataset/tsdf_fusion_data_$scan
