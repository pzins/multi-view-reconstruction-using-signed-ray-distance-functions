import argparse

import numpy as np
import torch
from tqdm import tqdm

from utils import *

parser = argparse.ArgumentParser(description='Clean Mesh')
parser.add_argument('-i', '--input', action='store', required=True, type=str, help='Input file')
parser.add_argument('-d', '--dataset', action='store', required=True, type=str, help='Dataset folder')
parser.add_argument('-s', '--subject', action='store', required=True, type=str, help='Subject')
parser.add_argument('--rescale', action='store', default=1, type=float, help='Rescale')
parser.add_argument('-dt', '--data_type', action='store', default="RP", type=str, help='[RP, DTU, Kinovis]')
args = parser.parse_args()

if args.data_type == "DTU":
    cams = list(range(1,50))
else:
    print("Not implemented")
    exit(0)


# No cleanning
if args.subject in ["scan055", "scan065", "scan069", "scan083", "scan097", "scan105", "scan106", "scan114", "scan118", "scan122"]:
    exit(0)
elif args.subject in ["scan024", "scan037", "scan040", "scan063"]:
    dilate = 2
elif args.subject == "scan110":
    dilate = 0

print("Number of cameras:", len(cams))

rescale = None
if args.rescale != 1:
    rescale = args.rescale
data = load_data(args.data_type, args.dataset, args.subject, cams, load_mask=True, load_mesh=False, load_image=False, rescale=rescale, dilate_mask=2)
calib_tensor = torch.from_numpy(data["calib"])
proj_tensor = data["proj"]
mask_tensor = torch.from_numpy(data["mask"])


K = torch.from_numpy(proj_tensor)


mesh = trimesh.load(args.input)

# Create mask of triangle center that project on the silhouettes
centers = mesh.vertices
centers = np.hstack((centers, np.ones((centers.shape[0],1)))).T
centers = torch.from_numpy(centers).float()

batch_size = 10000
num_batchs = centers.shape[1] // batch_size + 1

tot = []
for i in tqdm(range(num_batchs)):
    pts = centers[:,i*batch_size:(i+1)*batch_size]
    ref_points = world_to_view_persp(pts, calib_tensor, K, data_type=args.data_type)
    ref_points = ref_points.permute(0,2,1)
    if args.data_type != "RP":
        ref_points[..., 0] = (ref_points[..., 0] / (mask_tensor.shape[2] / 2)) - 1
        ref_points[..., 1] = (ref_points[..., 1] / (mask_tensor.shape[1] / 2)) - 1
    res = torch.nn.functional.grid_sample(mask_tensor[:,None,:,:], ref_points[:,:,None,:2], align_corners=True, mode="bilinear")
    mask = (ref_points[...,0] > 1) | (ref_points[...,0] < -1) | (ref_points[...,1] > 1) | (ref_points[...,1] < -1)
    res = res[:,0,:,0]
    res[mask] = 1
    tot.append(res.prod(0).squeeze())


res = np.concatenate(tot)
res = res[:,None].repeat(3,1).astype(bool)
pts = mesh.vertices[res].reshape(-1,3)
pts = pts.astype(np.float32)
p = trimesh.points.PointCloud(vertices=pts, colors=None)
p.export(args.input.replace(".obj", "_cleaned.obj").replace(".ply", "_cleaned.ply").replace(".xyz", "_cleaned.ply"))