import argparse
import os

import numpy as np
import torch
from tqdm import tqdm

from utils import *

parser = argparse.ArgumentParser(description='Pointcloud fusion')
parser.add_argument('-d', '--dataset', action='store', required=True, type=str, help='Dataset folder')
parser.add_argument('-i', '--input_folder', action='store', required=True, type=str, help='Input folder')
parser.add_argument('--z_thresh', action='store', type=float, default=None, help='Pointcloud fusion depth threshold')
parser.add_argument('--consistency', action='store', type=float, default=None, help='Pointcloud fusion number of views consistency threshold')
parser.add_argument('--batch_size', action='store', type=float, default=100000, help='Fusion batch size')
parser.add_argument('--show', action='store_true', help='Show with Open3D')
parser.add_argument('--pp', action='store_true', help='Use post-processed depthmaps')
parser.add_argument('-dt', '--data_type', action='store', default="RP", type=str, help='[RP, DTU, Kinovis]')
parser.add_argument('--rescale', action='store', default=1, type=float, help='Rescale')
args = parser.parse_args()

subject = find_subject(args.input_folder, args.data_type)

# Cameras
if args.data_type == "DTU":
    cams = list(range(1,50))

    d = {
        "scan024": [0.1, 2],
        "scan037": [0.2, 2],
        "scan040": [0.1, 2],
        "scan055": [0.1, 2],
        "scan063": [0.05, 3],
        "scan065": [0.1, 2],
        "scan069": [0.1, 2],
        "scan083": [0.1, 4],
        "scan097": [0.1, 3],
        "scan105": [0.1, 2],
        "scan106": [0.1, 2],
        "scan110": [0.05, 2],
        "scan114": [0.1, 3],
        "scan118": [0.1, 2],
        "scan122": [0.1, 2],
    }
    z_thresh = d[subject][0]
    consistency = d[subject][1]

elif args.data_type == "Kinovis":
    cams = list(range(65))
    cams = list(range(0,67))
    to_remove = []
    for i in cams:
        if not os.path.isfile(os.path.join(args.input_folder, "%d_dist.npy" % i)):
            to_remove.append(i)
    for i in to_remove:
        cams.remove(i)

elif args.data_type == "RP":
    cams = list(range(342,360,2)) + list(range(0,20,2))

elif args.data_type == "BlendedMVS":
    if subject == "scene_99":
        cams = list(range(14)) # scene_99
        z_thresh = 0.001
        consistency = 2
    elif subject == "scene_51":
        cams = list(range(36)) # scene_51
        z_thresh = 0.0005
        consistency = 4
    else:
        print("Unknown subject")
        exit(0)

use_cuda = False
use_cuda = True
device = torch.device("cuda" if use_cuda else "cpu")

rescale = None
if args.rescale != 1:
    rescale = args.rescale

print("Load calibration and images")
data = load_data(args.data_type, args.dataset, subject, cams, load_mask=False, load_mesh=False, rescale=rescale)
calib_tensor = data["calib"]
proj_tensor = data["proj"]
render_tensor = data["image"]

res_w = render_tensor.shape[2]
res_h = render_tensor.shape[1]

calib_tensor = torch.tensor(calib_tensor).to(device)
proj_tensor = torch.tensor(proj_tensor).to(device)
render_tensor = torch.tensor(render_tensor).to(device)



print("Load depthmaps")
depthmaps = []
for cam in tqdm(range(len(cams))):
    if args.pp:
        depthmaps.append(np.load(os.path.join(args.input_folder, str(cams[cam]) + "_pp.npy")))
    else:
        depthmaps.append(np.load(os.path.join(args.input_folder, str(cams[cam]) + ".npy")))
depthmaps = np.stack(depthmaps).astype(np.float32)
depthmaps = torch.tensor(depthmaps).to(device)



print("Compute normals")
normals = []
for i in tqdm(range(len(cams))):
    offset = 1
    xp = torch.roll(depthmaps[cam], offset, dims=0)
    xm = torch.roll(depthmaps[cam], -offset, dims=0)
    yp = torch.roll(depthmaps[cam], offset, dims=1)
    ym = torch.roll(depthmaps[cam], -offset, dims=1)
    dzdx = (xp-xm)/2
    dzdy = (yp-ym)/2
    direction = torch.cat([-dzdx, -dzdy, torch.ones(dzdx.shape).to(device)])
    direction = direction.reshape(3,-1)
    direction = direction / torch.linalg.norm(direction, axis=0)
    direction = direction.reshape(3, res_h, res_w).unsqueeze(0)
    normals.append(direction)
normals = torch.cat(normals)

world_normals = calib_tensor[:,:3,:3].permute(0,2,1) @ normals.reshape(normals.shape[0], 3, -1)
world_normals = world_normals.reshape(world_normals.shape[0], 3, res_h, res_w)


# Set parameters
if args.z_thresh is not None:
    z_thresh = args.z_thresh
if args.consistency is not None:
    consistency = args.consistency




print("Start pointcloud fusion - consistency %d - z_thresh %f" % (consistency, z_thresh))
fused_pts = []
fused_rgb = []
normals_all = []
all_idx = torch.arange(len(cams))
all_valid = []
for ref_idx in tqdm(range(len(cams))):
    src_idx = all_idx != ref_idx
    n_pts = res_h * res_w

    pts_x = np.linspace(0, res_w - 1, res_w)
    pts_y = np.linspace(0, res_h - 1, res_h)
    pts_xx, pts_yy = np.meshgrid(pts_x, pts_y)
    pts_init = torch.from_numpy(np.stack((pts_xx, pts_yy, np.ones_like(pts_xx)), axis=0).astype(np.float32)).float().to(device)

    if args.data_type == "RP":
        K = get_K_from_proj(proj_tensor.cpu().numpy(), h=res_h, w=res_w).astype(np.float32)
        K = torch.from_numpy(K).to(device)
    else:
        K = proj_tensor
    
    pts_init = calib_tensor[ref_idx,:3,:3].T @ (torch.linalg.inv(K[ref_idx,:3,:3]) @ (pts_init * depthmaps[ref_idx].unsqueeze(0)).view(3,-1)) - calib_tensor[ref_idx,:3,:3].T @ calib_tensor[ref_idx,:3,3, None]
        
    valid_per_src_list = []
    pts_sample_list = []
    n_valid_list = []
    num_batchs = pts_init.shape[1] // args.batch_size + 1

    for i in range(num_batchs):
        pts = pts_init[:,i*args.batch_size:(i+1)*args.batch_size]

        pts_sample_all = []
        valid_per_src_all = []
        
        pts_reproj = torch.bmm(calib_tensor[:, :3, :3], pts.unsqueeze(0).repeat(calib_tensor.shape[0], 1, 1)) + calib_tensor[:, :3, 3, None]
        pts_reproj = torch.bmm(K[:,:3,:3], pts_reproj)
        z_reproj = pts_reproj[:, 2]
        pts_reproj = pts_reproj / z_reproj.unsqueeze(1)

        valid_z = (z_reproj > 1e-4)
        valid_x = (pts_reproj[:, 0] >= 0.) & (pts_reproj[:, 0] <= float(res_w - 1))
        valid_y = (pts_reproj[:, 1] >= 0.) & (pts_reproj[:, 1] <= float(res_h - 1))

        grid = torch.clone(pts_reproj[:, :2]).transpose(2, 1)
        n_pts = pts.shape[-1]
        grid = grid.view(len(cams), n_pts, 1, 2)
        grid[..., 0] = (grid[..., 0] / float(res_w - 1*0)) * 2 - 1.0  # normalize to [-1, 1]
        grid[..., 1] = (grid[..., 1] / float(res_h - 1*0)) * 2 - 1.0  # normalize to [-1, 1]
        z_sample = torch.nn.functional.grid_sample(depthmaps.unsqueeze(1), grid, mode='nearest', align_corners=True, padding_mode='zeros')
        z_sample = z_sample.squeeze(1).squeeze(-1)

        z_diff = torch.abs(z_reproj - z_sample)
        valid_disp = z_diff < z_thresh

        valid_per_src = (valid_disp & valid_x & valid_y & valid_z)
        valid_per_src[ref_idx] = 0
        n_valid = torch.sum(valid_per_src.int(), dim=0)

        pts_sample = torch.bmm(torch.linalg.inv(K[:,:3,:3]), pts_reproj * z_sample.unsqueeze(1))
        pts_sample = torch.bmm(calib_tensor[:, :3, :3].transpose(2, 1), pts_sample - calib_tensor[:, :3, 3, None])


        pts_sample_all.append(pts_sample)
        valid_per_src_all.append(valid_per_src)
        pts_sample_all = torch.cat(pts_sample_all, dim=0)
        valid_per_src_all = torch.cat(valid_per_src_all, dim=0)

        pts_sample_all = pts_sample
        valid_per_src_all = valid_per_src_all

        valid = n_valid >= consistency

        pts_avg = pts
        for i in range(len(cams)):
            if i == ref_idx:
                continue
            pts_sample_i = pts_sample_all[i]
            invalid_idx = torch.isnan(pts_sample_i)
            pts_sample_i[invalid_idx] = 0.
            valid_i = valid_per_src_all[i] & ~torch.any(invalid_idx, dim=0)
            pts_avg += pts_sample_i * valid_i.float().unsqueeze(0)

        pts_avg = pts_avg / (n_valid + 1).float().unsqueeze(0).expand(3, n_pts)

        pts_filtered = pts_avg.transpose(1, 0)[valid]

        pts_reproj = torch.bmm(calib_tensor[:, :3, :3], pts_filtered.T.unsqueeze(0).repeat(calib_tensor.shape[0], 1, 1)) + calib_tensor[:, :3, 3, None]
        pts_reproj = torch.bmm(K[:,:3,:3], pts_reproj)
        z_reproj = pts_reproj[:, 2]
        pts_reproj = pts_reproj / z_reproj.unsqueeze(1)
        grid = torch.clone(pts_reproj[:, :2]).transpose(2, 1)
        grid = grid.view(len(cams), -1, 1, 2)
        grid[..., 0] = (grid[..., 0] / float(res_w - 1*0)) * 2 - 1.0  # normalize to [-1, 1]
        grid[..., 1] = (grid[..., 1] / float(res_h - 1*0)) * 2 - 1.0  # normalize to [-1, 1]
        normals = torch.nn.functional.grid_sample(world_normals, grid, mode='nearest', align_corners=True, padding_mode='zeros')
        
        normals = normals[ref_idx][:,:,0]
        normals_all.append(normals.T.cpu().numpy())

        pts_filtered = pts_filtered.cpu().numpy()
        fused_pts.append(pts_filtered)

fused_pts = np.concatenate(fused_pts, axis=0)
normals = np.concatenate(normals_all, axis=0)
print("After point-cloud fusion:", fused_pts.shape)

# Filter outliers
if args.data_type == "Kinovis":
    threshold = 5
elif args.data_type == "BlendedMVS":
    threshold = 5
elif args.data_type == "RP":
    threshold = 500
elif args.data_type == "DTU":
    threshold = 500
if subject != "scene_99":
    m = (abs(fused_pts).mean(1) < threshold)
    fused_pts = fused_pts[m]
    normals = normals[m]
print("After filtering:", fused_pts.shape)


print("After down sampling", fused_pts.shape)
p = trimesh.points.PointCloud(vertices=fused_pts, colors=None)
print("Exporting...")

outname = "fused.ply"
p.export(os.path.join(args.input_folder, outname))
