import glob
import os
import shutil
from distutils.dir_util import copy_tree

from tqdm import tqdm

# Parameters
# ------------------------------------------------------------------------------------------------
# Set the path to masks (can be obtained from https://github.com/lioryariv/idr)
idr_data = "/disk/data/DTU_idr"
# Set the path to DTU original data
dtu_data = "/disk/data/DTU/"



# Training scans to train the photoconsistency network
scans = list(set(range(1, 129)) - set([78, 79, 80, 81])) # all meshes
out_folder = "./data/dataset_dtu_full/"
copy_mesh = True
"""

# Test scans
scans = [24, 37, 40, 55, 63, 65, 69, 83, 97, 105, 106, 110, 114, 118, 122]
out_folder = "./data/dataset_dtu/"
copy_mesh = False
"""


copy_calib = True
copy_images = True
copy_masks = True
# ------------------------------------------------------------------------------------------------




os.makedirs(out_folder, exist_ok=True)
os.makedirs(os.path.join(out_folder, "images"), exist_ok=True)
os.makedirs(os.path.join(out_folder, "masks"), exist_ok=True)
os.makedirs(os.path.join(out_folder, "calib"), exist_ok=True)
os.makedirs(os.path.join(out_folder, "mesh_mvpifu"), exist_ok=True)
os.makedirs(os.path.join(out_folder, "mesh_gt"), exist_ok=True)



copy_tree(os.path.join(dtu_data, "SampleSet/MVS Data/ObsMask"), os.path.join(out_folder, "ObsMask"))



for scan in tqdm(scans):
    out_calib = os.path.join(out_folder, "calib", "scan%03d" % scan)
    out_images = os.path.join(out_folder, "images", "scan%03d" % scan)
    out_masks = os.path.join(out_folder, "masks", "scan%03d" % scan)
    out_mesh_mvpifu = os.path.join(out_folder, "mesh_mvpifu", "scan%03d" % scan)
    out_mesh_gt = os.path.join(out_folder, "mesh_gt", "scan%03d" % scan)

    os.makedirs(out_images, exist_ok=True)
    os.makedirs(out_masks, exist_ok=True)
    os.makedirs(out_calib, exist_ok=True)
    os.makedirs(out_mesh_mvpifu, exist_ok=True)
    os.makedirs(out_mesh_gt, exist_ok=True)

    if copy_mesh:
        in_mesh_gt = os.path.join(dtu_data, "Surfaces/spsr/poisson_%03d_trimmed.ply" % scan)
        shutil.copyfile(in_mesh_gt, os.path.join(out_mesh_gt, os.path.basename(in_mesh_gt)))
        # continue

    if copy_calib:
        in_calib = os.path.join(dtu_data, "SampleSet/MVS Data/Calibration/cal18/")
        files = glob.glob(os.path.join(dtu_data, in_calib, "pos*.txt"))
        for file in files:
            shutil.copyfile(file, os.path.join(out_calib, os.path.basename(file)))
    if copy_images:
        in_images = os.path.join(dtu_data, "Rectified/scan%d/" % scan)
        files = glob.glob(os.path.join(dtu_data, in_images, "*.png"))
        # files = glob.glob(os.path.join(dtu_data, in_images, "*max.png"))
        for file in files:
            shutil.copyfile(file, os.path.join(out_images, os.path.basename(file)))
    if copy_masks:
        in_masks = os.path.join(idr_data, "scan%d/mask/" % scan)
        files = glob.glob(os.path.join(dtu_data, in_masks, "*.png"))
        for file in files:
            out_name = "%03d.png" % (int(os.path.basename(file)[:-4]) + 1)
            shutil.copyfile(file, os.path.join(out_masks, out_name))
