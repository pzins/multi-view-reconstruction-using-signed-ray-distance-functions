import os

import numpy as np
import trimesh
from PIL import Image
from tqdm import tqdm


def load_data_RP(base_path, subject, cams, mesh_folder="mesh_gt", load_mesh=True, load_image=True, load_mask=True, load_calib=True, rescale=None):
    render_list = []
    mask_list = []
    calib_list = []
    proj_list = []
    rot_list = []
    view_list = []
    
    for cam in tqdm(cams):
        param_path = os.path.join(base_path, "PARAM", subject, '%d_0_00.npy' % cam)
        render_path = os.path.join(base_path, "RENDER", subject, '%d_0_00.jpg' % cam)
        
        if load_image:
            render = Image.open(render_path).convert('RGB')
            if rescale is not None:
                new_w = int(render.size[0] * rescale)
                new_h = int(render.size[1] * rescale)
                render = render.resize((new_w, new_h), Image.ANTIALIAS)

            arr = np.array(render) / 255.0
            render_list.append(arr)

        if load_mask:
            mask_path = os.path.join(base_path, "MASK", subject, '%d_0_00.png' % cam)
            mask = Image.open(mask_path).convert('L')

            if rescale is not None:
                new_w = int(mask.size[0] * rescale)
                new_h = int(mask.size[1] * rescale)
                mask = mask.resize((new_w, new_h), Image.ANTIALIAS)
            
            mask = np.array(mask) / 255.0

            mask_list.append(mask)
        
        if load_calib:

            param = np.load(param_path, allow_pickle=True)
            norm = param.item().get('norm')
            proj = param.item().get('proj')
            view = param.item().get('view')
            rot = view[:3,:3]
            calib = view @ norm

            calib_list.append(calib)
            proj_list.append(proj)
            rot_list.append(rot)
            view_list.append(view)
    
    res = {}
    
    if load_mesh:
        mesh_file = os.path.join(base_path, mesh_folder, subject, subject + "_100k.obj")
        if not os.path.isfile(mesh_file):
            mesh_file = mesh_file.replace(".obj", ".ply")
        mesh = trimesh.load(mesh_file)
        res["mesh"] = mesh
    
    if len(calib_list) > 0:
        calib_tensor = np.stack(calib_list, 0).astype(np.float32)
        proj_tensor = np.stack(proj_list, 0).astype(np.float32)
        rot_tensor = np.stack(rot_list, 0).astype(np.float32)
        view_tensor = np.stack(view_list, 0).astype(np.float32)
        res["calib"] = calib_tensor
        res["proj"] = proj_tensor
        res["rot"] = rot_tensor
        res["view"] = view_tensor

    if len(render_list) > 0:
        render_tensor = np.stack(render_list, 0).astype(np.float32)
        res["image"] = render_tensor
    
    if len(mask_list) > 0:
        mask_tensor = np.stack(mask_list, 0).astype(np.float32)
        res["mask"] = mask_tensor

    return res