import numpy as np
import torch

from PhotoDataset import *
from utils import *


def photo_learned(samples, calib_tensor, calib_inv, proj_tensor, render_tensor, cam,
                 model, features_list, std_c, curr_num_rays, config, dbg, it):
    num_samples = samples.shape[1]
    if model.window > 0:
        points = samples
        # in points: [4096, 51, 4]
        limit = 10
        nb_points = points.shape[0]
        samples_per_ray = points.shape[1]

        points = points.reshape(points.shape[0] * points.shape[1], -1).T
        points = calib_tensor[cam] @ points
        points = points.T
        points = points.reshape(nb_points, samples_per_ray, -1)
        points = torch.repeat_interleave(points, (limit * 2 + 1)**2, 0)


        dist = 10
        if config["data_type"] == "BlendedMVS":
            dist = 0.01
        elif config["data_type"] == "Kinovis":
            dist = 0.02
        elif config["data_type"] == "RP":
            dist = 1
        else:
            dist = 0.02
        # dist = 0.02
        # dist = 0.03
        # dist = 0.02
        # dist = 10
        offset = get_pixels_torch("cuda", -limit, limit+1, -limit, limit+1, 1)[:,:3]
        offset[:,2] = 0
        offset /= limit
        offset *= dist
        offset = offset.repeat(nb_points, 1)
        offset = offset[:, None, :]
        points[:,:,:3] = points[:,:,:3] + offset
        
        s0 = points.shape[0]
        s1 = points.shape[1]
        points = points.reshape(points.shape[0] * points.shape[1], -1).T
        points = calib_inv[cam] @ points
        points = points.T
        points = points.reshape(s0, s1, -1)
        samples = points
        # out points: [495616, 51, 4]
        samples = samples.permute(1,0,2)


    ref_points = world_to_view_persp(samples.reshape(samples.shape[0] * samples.shape[1], samples.shape[2]).T, calib_tensor, proj_tensor, config["data_type"])
    ref_points = ref_points.permute(0,2,1)
    ref_points = ref_points.view(ref_points.shape[0], ref_points.shape[1], 1, 4)
    ref_points = ref_points[:,:,:,:2]

    if config["data_type"] != "RP":
        ref_points[..., 0] = (ref_points[..., 0] / (render_tensor.shape[2] / 2)) - 1
        ref_points[..., 1] = (ref_points[..., 1] / (render_tensor.shape[1] / 2)) - 1
    
    if model.window > 0:
        pred = model(render_tensor.half(), ref_points.half())
        colors = pred.view(-1, curr_num_rays, 1).permute(1,0,2)
    else:
        pred = model.predict(features_list, ref_points.half()) # PhotoNet_orig
        colors = pred.view(curr_num_rays, -1, 1)
    
    colors = torch.exp(-(1-colors)**2 / (2 * std_c**2))

    return colors








def photo_median(samples, calib_tensor, proj_tensor, render_tensor, config, std_c):
    curr_num_rays = samples.shape[0]
    num_samples = samples.shape[1]
    

    ci = get_color_torch(samples.reshape(samples.shape[0] * samples.shape[1], samples.shape[2]).T, calib_tensor, proj_tensor, render_tensor, data_type=config["data_type"])

    cm = ci.median(1, keepdim=True)[0]

    photo_err_i_j = photometric_error(ci, cm)
    photo_err_i_j /= np.sqrt(ci.shape[-1])

    colors = torch.exp(-photo_err_i_j**2 / (2 * std_c**2))
    colors = colors.reshape(curr_num_rays, num_samples, -1)
    color_cste = 1
    colors = torch.prod(colors + color_cste, dim=2, keepdim=True)
    return colors