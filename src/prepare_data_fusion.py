import argparse
import os
from multiprocessing import Pool

import cv2
import numpy as np

from utils import *

parser = argparse.ArgumentParser(description='Convert distances maps to depth maps')
parser.add_argument('-i', '--input_folder', action='store', required=True, type=str, help='Input folder')
parser.add_argument('-d', '--dataset', action='store', required=True, type=str, help='Dataset folder')
parser.add_argument('-dt', '--data_type', action='store', default="RP", type=str, help='[RP, DTU, Kinovis]')
parser.add_argument('--rescale', action='store', default=1, type=float, help='Rescale')
parser.add_argument('-o', '--out_folder', action='store', default="tsdf_fusion_data", type=str, help='Out folder for tsdf_fusion')
args = parser.parse_args()

subject = find_subject(args.input_folder, args.data_type)

# Cameras
if args.data_type == "RP":
    cams = list(range(342,360,2)) + list(range(0,20,2))

elif args.data_type == "Kinovis":
    cams = list(range(0,67))
    to_remove = []
    for i in cams:
        if not os.path.isfile(os.path.join(args.input_folder, "%d_dist.npy" % i)):
            to_remove.append(i)
    for i in to_remove:
        cams.remove(i)

elif args.data_type == "DTU":
    if int(subject[4:]) < 82:
        cams = list(range(1,50))
    else:
        cams = list(range(1,65))
    cams = list(range(1,50))


elif args.data_type == "BlendedMVS":
    if subject == "scene_99":
        cams = list(range(14))
    elif subject == "scene_51":
        cams = list(range(36))
    else:
        print("Unknown subject")
        exit(0)

else:
    print("Unknown data type")
    exit(0)


# Load data
data = load_data(args.data_type, args.dataset, subject, cams, load_image=True, load_mesh=False, load_mask=True, rescale=args.rescale)
calib_tensor = data["calib"]
proj_tensor = data["proj"]
mask_tensor = data["mask"]
cam_centers = np.linalg.inv(calib_tensor)[:,:,3]



# Convert distance along rays to depth
def convert_distance_to_depth(cam):
    distance = np.load(os.path.join(args.input_folder, str(cams[cam]) + "_dist.npy"))
    height = distance.shape[0]
    width = distance.shape[1]
    
    distance[mask_tensor[cam] == 0] = DEPTH_NO_SURFACE
    distance = distance.reshape(-1,1)

    coo = get_pixels(0, height, 0, width, 1)
    coo[:,:2] += 0.5

    _, directions = ray_directions_and_origins(coo, proj_tensor, calib_tensor, cam, cam_centers, w=height, h=width, is_opengl=(args.data_type=="RP"))
    depth = distances_to_depth(calib_tensor[cam], directions, distance, args.data_type)
    
    depth = depth.reshape(height, width)

    np.save(os.path.join(args.input_folder, str(cams[cam]) + ".npy"), depth.astype(np.float32))

pool = Pool(os.cpu_count())
pool.map(convert_distance_to_depth, list(range(len(cams))))



# Prepare data for TSDF Fusion
os.makedirs(os.path.join(args.dataset, args.out_folder), exist_ok=True)

extrinsics_tensor = data["calib"] 
intrinsics_tensor = data["proj"]
render_tensor = data["image"]

def prepare_data(i):
    cam = cams[i]
    # data = np.load(os.path.join(args.input_folder, str(cam) + "_pp.npy"))
    data = np.load(os.path.join(args.input_folder, str(cam) + ".npy"))
    data = cv2.bilateralFilter(data, 5, 5, 9)
    data[data >= 1000] = 0
    np.save(os.path.join(args.input_folder, str(cam) + "_pp.npy"), data)

    
    if args.data_type == "Kinovis" or args.data_type == "BlendedMVS":
        data *= 100

    data *= 10 # Convert into mm
    data = data.astype(np.uint32)

    im = Image.fromarray(data)
    
    im.save(os.path.join(args.dataset, args.out_folder, "frame-%06d.depth" % i + ".png"))

    im = Image.fromarray((render_tensor[i] * 255).astype(np.uint8))
    im.save(os.path.join(args.dataset, args.out_folder, "frame-%06d.color" % i + ".jpg"))

    proj = intrinsics_tensor[i]
    if args.data_type == "RP":
        h = w = data.shape[0]
        fenetrage = np.array([[w/2, 0, w/2], [0, -h/2, h/2], [0,0,1]])
        proj_ = np.vstack((proj[:2,:3], proj[3,:3]))
        K = (fenetrage @ proj_)
        K = np.vstack((np.hstack((K, np.zeros((3,1)))), np.array([0,0,0,1])))[None,:,:]
    else:
        K = proj[None,:,:]
        K[:,0,0] *= args.rescale
        K[:,1,1] *= args.rescale
        K[:,0,2] *= args.rescale
        K[:,1,2] *= args.rescale

    Rt_inv = np.linalg.inv(extrinsics_tensor[i])
    if args.data_type == "Kinovis" or args.data_type == "BlendedMVS":
        Rt_inv[:3,3] *= 100
    np.savetxt(os.path.join(args.dataset, args.out_folder, "frame-%06d.pose" % i + ".txt"), Rt_inv)
    
    if i == 0:
        np.savetxt(os.path.join(args.dataset, args.out_folder, "camera-intrinsics.txt"), K[0, :3, :3])
    np.savetxt(os.path.join(args.dataset, args.out_folder, "frame-%06d.intrinsics" % i + ".txt"), K[0, :3, :3])

pool = Pool(os.cpu_count())
results = pool.map(prepare_data, list(range(len(cams))))