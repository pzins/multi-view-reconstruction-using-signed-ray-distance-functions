import os
import socket

if "charmeleon" not in socket.gethostname() and "Jynx" not in socket.gethostname() and "lapras" not in socket.gethostname() and "geyser" not in socket.gethostname():
    mpl_dir = "/services/morpheo/data/.cache_mpl"
    os.makedirs(mpl_dir, exist_ok=True)
    os.environ["MPLCONFIGDIR"] = mpl_dir
import random
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import PIL.Image
import torch
from matplotlib.widgets import Cursor

from dataset_blendedmvs import *
from dataset_dtu import *
from dataset_rp import *

# =========================================================================
DEPTH_NO_SURFACE = 10000
np.random.seed(10)
random.seed(10)
torch.set_printoptions(precision=12, sci_mode=True, linewidth=200)
COLORS = ["b","g", "r", "black", "yellow", "#bbaa22", "#ee8811", "#11ee11"]
# =========================================================================


def load_data(dataset, base_path, subject, cams, mesh_folder="mesh_gt", load_mesh=True, load_image=True, load_mask=True, load_calib=True, rescale=None, erode_mask=-1, dilate_mask=-1, random_lights=False):
    if rescale == 1:
        rescale = None
    if dataset == "RP":
        return load_data_RP(base_path=base_path, subject=subject, cams=cams, mesh_folder=mesh_folder, load_mesh=load_mesh, load_image=load_image, load_mask=load_mask, load_calib=load_calib, rescale=rescale)
    elif dataset == "DTU":
        return load_data_DTU(base_path=base_path, subject=subject, cams=cams, mesh_folder=mesh_folder, load_mesh=load_mesh, load_image=load_image, load_mask=load_mask, load_calib=load_calib, rescale=rescale, erode_mask=erode_mask, dilate_mask=dilate_mask, random_lights=random_lights)
    elif dataset == "Kinovis":
        return load_data_Kinovis(base_path=base_path, subject=subject, cams=cams, mesh_folder=mesh_folder, load_mesh=load_mesh, load_image=load_image, load_mask=load_mask, load_calib=load_calib, rescale=rescale, erode_mask=erode_mask, dilate_mask=dilate_mask)
    elif dataset == "BlendedMVS":
        return load_data_BlendedMVS(base_path=base_path, subject=subject, cams=cams, mesh_folder=mesh_folder, load_mesh=load_mesh, load_image=load_image, load_mask=load_mask, load_calib=load_calib, rescale=rescale, erode_mask=erode_mask, dilate_mask=dilate_mask)
    else:
        print("Unknown dataset:", dataset)
        exit(0)




def get_cameras_cluster(base_path, scene, pair_file=None, nb_cams=None):
    p = os.path.join(base_path, "cams", scene, "pair.txt")
    if pair_file is not None:
        p = pair_file
    data = open(p).readlines()
    total_cams = int(data[0])

    res = {}
    i = 1
    while i < total_cams*2:
        idx = int(data[i])
        i += 1
        tmp = data[i].split(" ")
        i += 1

        num_cams = int(tmp[0])
        other_cams = []
        for j in range(num_cams):
            other_cams.append(int(tmp[1+j*2]))

        if nb_cams is not None:
            res[idx] = other_cams[:nb_cams]
        else:
            res[idx] = other_cams

    return res



def plot(xy, img, is_opengl=False, filename=None, dpi=2000):
    # xy : (4, N)
    # img: (H, W, 3)
    if is_opengl:
        resized_xy = (xy + 1) * 0.5
        resized_xy[0,:] *= img.shape[1]
        resized_xy[1,:] *= img.shape[0]

    else:
        resized_xy = xy
    fig = plt.figure()
    plt.imshow(img)
    # plt.scatter(resized_xy[0, :], resized_xy[1, :], s=5, marker='o', c="red")
    plt.scatter(resized_xy[0, :], resized_xy[1, :], s=0.01, marker='.', c="red")
    if filename is not None:
        plt.savefig(filename, dpi=dpi)
    else:
        plt.show()
    plt.close()


def export_points(filename, points, colors=None):
    # points : (N, 3/4)
    if points.shape[-1] == 4:
        points = points[:, :3]
    data = np.hstack((np.array(["v"] * points.shape[0]).reshape(-1,1), points))
    if colors is not None:
        data = np.hstack((data, colors))
        np.savetxt(filename, data,  fmt="%s %s %s %s %s %s %s", delimiter=" ")
    else:
        np.savetxt(filename, data,  fmt="%s %s %s %s", delimiter=" ")
    


def get_color_torch(points, calib_tensor, proj_tensor, images, data_type="RP"):
    # points: (4, N)
    # images: (K, H, W, C)
    # calib_tensor / proj_tensor: (K, 4, 4)
    use_numpy = not torch.is_tensor(images)
    if points.shape[1] == 3:
        points = np.vstack((points, np.ones((1, points.shape[1]))))
    pts = world_to_view_persp(points, calib_tensor, proj_tensor, data_type)
    # pts : (K, 4, N)
    if use_numpy:
        images_tensor = torch.from_numpy(images).permute(0,3,1,2)
    else:
        images_tensor = images.permute(0,3,1,2)
    if use_numpy:
        pts_tensor = torch.from_numpy(pts)[:,:2,:].unsqueeze(1).permute(0,1,3,2)
    else:
        pts_tensor = pts[:,:2,:].unsqueeze(1).permute(0,1,3,2)

    if data_type != "RP":
        pts_tensor[..., 0] = (pts_tensor[..., 0] / (images_tensor.shape[3] / 2)) - 1
        pts_tensor[..., 1] = (pts_tensor[..., 1] / (images_tensor.shape[2] / 2)) - 1

    # Debug projection
    # for i in range(pts.shape[0]):
    #     plot(pts_tensor[i,0].T.cpu().numpy(), images[i].cpu().numpy(), is_opengl=True, filename="./data/project_pts_image_%d.png" % i, dpi=150)
    # exit(0)

    res = torch.nn.functional.grid_sample(images_tensor, pts_tensor, align_corners=True, mode="bilinear")
    # res : (K,3,1,N)
    res = res.permute(3,0,1,2).squeeze(3)
    if use_numpy:
        res = res.numpy()
    return res


def plot_cost(data, x=None, block=True, filename=None, v_axis=None, v_axis2=None):
    # data : [(N,)]
    fig = plt.figure()
    ax = fig.add_axes([0.1, 0.1, 0.8, 0.8])
    if x is None:
        x = np.arange(data[0].shape[0])
    if v_axis is not None:
        plt.axvline(x=v_axis, c="blue", lw=0.3)
    if v_axis2 is not None:
        plt.axvline(x=v_axis2, c="red", lw=0.3)
    for i,d in enumerate(data):
        plt.scatter(x, d, s=1, c=COLORS[i], marker = 'o')

    # for i,d in enumerate(data[1:]):
    #     plt.scatter(x, d, s=5, c=COLORS[i+1], marker = 'o')
    # plt.scatter(x, data[0], s=15, c=COLORS[0], marker = '.')
    cursor = Cursor(ax, useblit=True, color='red', linewidth=1)
    if 1 and filename is not None:
        plt.savefig(filename, dpi=300)
    else:
        plt.savefig(filename, dpi=300)
        plt.show(block = block)
    plt.close()




def world_to_view_persp(points, calib, proj, data_type="RP"):
    sample_points_view = proj @ calib @ points
    if data_type == "RP":
        sample_points_view[...,0,:] /= sample_points_view[...,3,:]
        sample_points_view[...,1,:] /= -sample_points_view[...,3,:]
        sample_points_view[...,2,:] /= sample_points_view[...,3,:]
    else:
        sample_points_view[...,:2,:] /= sample_points_view[...,2:3,:]
    return sample_points_view


def photometric_error(ci, cm):
    # ci : (N, K, 3)
    # cm : (N, K, 1)
    # res : (N, K)
    if torch.is_tensor(ci):
        res = torch.linalg.norm(ci - cm, axis=2)
    else:
        res = np.linalg.norm(ci - cm, axis=2)
    return res


def get_K_from_proj(proj, w=2048, h=2048):
    # proj: (K,4,4)
    # return K: (K,4,4)
    fenetrage = np.array([[w/2, 0, w/2], [0, -h/2, h/2], [0,0,1]])
    proj_ = np.concatenate((proj[:,:2,:3], proj[:,3:4,:3]),axis=1)
    K = (fenetrage[None,...] @ proj_)
    K = np.concatenate((K, np.zeros((K.shape[0],K.shape[1],1))), axis=2)
    K = np.concatenate((K, np.zeros((K.shape[0],1,K.shape[2]))), axis=1)
    K[:,3,3] = 1
    return K


def unproject(points, K, depth=1):
    # points: (N,4)
    # K: (1,4,4)
    # out unprojectedPoint: (1,4,N)

    unprojectedPoint = np.linalg.inv(K) @ points.T

    # Decompose
    """
    fx = K[0,0,0]
    fy = K[0,1,1]
    cx = K[0,0,2]
    cy = K[0,1,2]
    points[:,0] = (points[:,0]+cx) * depth / fx
    points[:,1] = (points[:,1]+cy) * depth / fy
    points[:,2] = -depth
    unprojectedPoint = (points.T)[None,:,:]
    """
    return unprojectedPoint



def ray_directions_and_origins(points, proj, calib, cam, cam_centers, w=2048, h=2048, is_opengl=True):
    if points.shape[1] == 2:
        points = np.hstack((points, np.ones((points.shape[0],2))))
    elif points.shape[1] == 3:
        points = np.hstack((points, np.ones((points.shape[0],1))))

    points[:,0] *= 1
    points[:,1] *= 1
    if is_opengl:
        K = get_K_from_proj(proj, h=h, w=w)
    else:
        K = proj

    unprojectedPoint = unproject(points, K[cam:cam+1])
    

    world = np.linalg.inv(calib[cam]) @ unprojectedPoint.T
    world = world.T

    directionVector = world - cam_centers[cam][None,:,None]
    directionVector = directionVector / np.linalg.norm(directionVector, axis=1)
    origins = np.repeat(cam_centers[cam][None,:], directionVector.shape[2],0)[:,:3]
    directions = directionVector[0,:3].T
    return origins, directions


def distances_to_depth(cam_calib_tensor, directions, distance, data_type="RP"):
    # cam_calib_tensor: (4,4)
    # directions: (num_hits, 3)
    # distance: (num_hits, 1)

    cam_centers = np.linalg.inv(cam_calib_tensor)[:3,3]
    surface_points = cam_centers + directions * distance
    surface_points = np.hstack((surface_points, np.ones((surface_points.shape[0],1))))
    pts = cam_calib_tensor @ surface_points.T
    if data_type == "RP":
        depth = -pts[2]
    else:
        depth = pts[2]
    return depth


def get_pixels(top=0, bottom=2048, left=0, right=2048, stride=1):
    x = np.arange(left, right, stride)
    y = np.arange(top, bottom, stride)
    xv, yv = np.meshgrid(x, y, indexing='xy')
    coo = np.vstack((xv.flatten(), yv.flatten())).T
    coo = np.hstack((coo, np.ones((coo.shape[0],1))))
    return coo

def get_pixels_torch(device, beginX=0, endX=2048, beginY=0, endY=2048, stride=1, indexing='ij'):
    x = torch.arange(beginX, endX, stride).to(device)
    y = torch.arange(beginY, endY, stride).to(device)
    xv, yv = torch.meshgrid(x, y, indexing=indexing)
    coo = torch.vstack((torch.flatten(yv), torch.flatten(xv))).T
    coo = torch.hstack((coo, torch.ones((coo.shape[0],1)).to(device)))
    return coo


def save_depthmaps(depthmaps, path, cams):
    os.makedirs(path, exist_ok=True)
    for i in range(len(cams)):
        np.save(os.path.join(path, str(cams[i]) + "_dist.npy"), depthmaps[i])



def compute_metrics(pred, gt, thresh=0.5):
    with torch.no_grad():
        vol_pred = pred > thresh
        vol_gt = gt > thresh
        # acc = accuracy_score(vol_gt.cpu().numpy().flatten(), vol_pred.cpu().numpy().flatten())
        # pre = precision_score(vol_gt.cpu().numpy().flatten(), vol_pred.cpu().numpy().flatten())
        # rec = recall_score(vol_gt.cpu().numpy().flatten(), vol_pred.cpu().numpy().flatten())

        inter = vol_pred & vol_gt
        inter_neg = ~vol_pred & ~vol_gt

        true_pos = inter.sum().float()
        true_neg = inter_neg.sum().float()

        false_pos = (vol_pred & torch.logical_xor(vol_pred, vol_gt)).sum().float()
        false_neg = (~vol_pred & torch.logical_xor(vol_pred, vol_gt)).sum().float()

        accuracy = (true_pos + true_neg) / (true_pos + false_pos + true_neg + false_neg)
        precision = true_pos / (true_pos + false_pos)
        recall = true_pos / (true_pos + false_neg)

        aaa=~vol_gt
        strict_pos = (pred > 0.8).sum().float() / vol_gt.sum().float()
        strict_neg = (pred < 0.2).sum().float() / aaa.sum().float()

        return accuracy, precision, recall, strict_pos, strict_neg


def find_subject(path, data_type="RP"):
    inside = None
    if data_type == "RP":
        pattern = "rp_"
    elif data_type == "DTU":
        pattern = "scan"
    elif data_type == "Kinovis":
        pattern = "subject"
        inside = "-"
    elif data_type == "BlendedMVS":
        pattern = "5"
        pattern = "scene_"
    else:
        print("find_subject(), pattern not found")
        exit(0)
    for sp in path.split("/"):
        if sp.startswith(pattern) or (inside is not None and inside in sp):
            return sp
    print("Subject name not found in path")
    exit(0)


def compute_samples(cam, num_samples, resX, resY, depthmaps, calib_inv, K_inv, cam_centers, scales, device, tolerance=0.8, data_type="RP", return_pix_depth=False):
    
    pix = get_pixels_torch(device, 0, resX, 0, resY, stride=1).float()
    pix[:,:2] += 0.5 # center of pixel instead of top-left corner
    
    mask = (depthmaps[cam] < DEPTH_NO_SURFACE * tolerance)
    mask = mask.view(-1)
    
    pix = pix[mask]
    depth = depthmaps[cam].detach().view(-1)[mask]

    unprojectedPoint = K_inv[cam] @ pix.T

    unprojectedPoint = torch.cat([unprojectedPoint, torch.ones((1, unprojectedPoint.shape[1])).to(device)], dim=0)
    world = calib_inv[cam] @ unprojectedPoint

    directionVector = world - cam_centers[cam:cam+1].T
    directionVector = directionVector / torch.linalg.norm(directionVector[:3,:], axis=0, keepdim=True)
    directionVector = directionVector[:3,:]
    direction = directionVector[None, :, :].repeat(num_samples, 1, 1)

    if data_type == "RP":
        z_axis = torch.tensor([0,0,-1]).float().to(device)
    else:
        z_axis = torch.tensor([0,0,1]).float().to(device)
    camera_axis = calib_inv[cam,:3,:3] @ z_axis

    unprojectedPoint[:3,:] *= (camera_axis[None,:] @ directionVector) * depth
    world = calib_inv[cam] @ unprojectedPoint
    

    # compute samples with -offset / +offset
    points = world[None,:3,:] + scales * direction
    points = torch.cat((points, torch.ones((points.shape[0],1,points.shape[2])).to(device)), axis=1)
    points = points.permute(2,0,1)

    if return_pix_depth:
        return points, [pix, depth]
    return points


def filter_blendedmvs_cluster(ref_cam, cluster, calib, nb):
    z = np.array([[0,0,1]])
    others = cluster[ref_cam]
    m = {}
    for j in range(len(others)):
        ref_idx = list(cluster.keys()).index(ref_cam)
        p1 = np.linalg.inv(calib[ref_idx])
        
        other_idx = list(cluster.keys()).index(others[j])

        p2 = np.linalg.inv(calib[other_idx])
        rz1 = p1[:3,:3] @ z.T
        rz2 = p2[:3,:3] @ z.T
        c = (rz1 * rz2).sum() / (np.linalg.norm(rz1) * np.linalg.norm(rz2))
        m[others[j]] = c

    sorted_m = dict(sorted(m.items(), key=lambda item: item[1], reverse=True))
    res = []
    cnt = 0
    for i in sorted_m:
        res.append(i)
        if len(res) == nb:
            break
    return res
