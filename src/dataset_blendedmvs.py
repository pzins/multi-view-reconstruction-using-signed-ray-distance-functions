import os

import numpy as np
import trimesh
from PIL import Image
from scipy import ndimage


def load_data_BlendedMVS(base_path, subject, cams, mesh_folder="textured_mesh", load_mesh=True, load_image=True, load_mask=True, load_calib=True, rescale=None, erode_mask=-1, dilate_mask=-1):

    # mesh, image, masked image, depthmaps, cams, pair.txt
    image_list = []
    mask_list = []
    extr_list = []
    intr_list = []
    mask_images = True
    mask_images = False

    for cam in cams:
        calib_path = os.path.join(base_path, "cams", subject, "%08d_cam.txt" % cam)
        image = os.path.join(base_path, "blended_images", subject, "%08d.jpg" % cam)
        mask = os.path.join(base_path, "blended_images", subject, "%08d_masked_clean.jpg" % cam)
        image_masked = os.path.join(base_path, "blended_images", subject, "%08d_masked.jpg" % cam)


        if load_mask:
            if os.path.isfile(mask):
                mask = Image.open(mask).convert('L')
            else:
                mask = Image.open(image_masked).convert('L')
            mask = np.array(mask) / 255.0

            mask[mask > 0] = 1
            if erode_mask > 0:
                mask = ndimage.binary_erosion(mask, iterations=erode_mask)
            elif dilate_mask > 0:
                mask = ndimage.binary_dilation(mask, iterations=dilate_mask)
            mask_list.append(mask)

        if load_image:
            if not mask_images:
                render = Image.open(image).convert('RGB')
            else:
                render = Image.open(image_masked).convert('RGB')
            arr = np.array(render) / 255.0
            image_list.append(arr)

        if load_calib:
            with open(calib_path, "r") as f:
                calib = load_cam_blended_mvs(f)
                Rt = calib[0]                
                K = np.eye(4)
                K[:3,:3] = calib[1, :3, :3]
            extr_list.append(Rt)
            intr_list.append(K)

    res = {}
    
    if load_mesh:
        filename = os.path.join(base_path, mesh_folder, subject, "mesh.obj")
        if not os.path.isfile(filename):
            filename = filename.replace(".obj", ".ply")
        mesh = trimesh.load(filename)
        res["mesh"] = mesh

    if load_image:
        render_tensor = np.stack(image_list, 0).astype(np.float32)
        res["image"] = render_tensor
    
    if load_mask:
        mask_tensor = np.stack(mask_list, 0).astype(np.float32)
        res["mask"] = mask_tensor

    if load_calib:
        extr_tensor = np.stack(extr_list, 0).astype(np.float32)
        intr_tensor = np.stack(intr_list, 0).astype(np.float32)
        res["proj"] = intr_tensor
        res["calib"] = extr_tensor
    
    return res


def load_cam_blended_mvs(file, interval_scale=1):
    """ read camera txt file """
    cam = np.zeros((2, 4, 4))
    words = file.read().split()
    # read extrinsic
    for i in range(0, 4):
        for j in range(0, 4):
            extrinsic_index = 4 * i + j + 1
            cam[0][i][j] = words[extrinsic_index]

    # read intrinsic
    for i in range(0, 3):
        for j in range(0, 3):
            intrinsic_index = 3 * i + j + 18
            cam[1][i][j] = words[intrinsic_index]

    if len(words) == 29:
        cam[1][3][0] = words[27]
        cam[1][3][1] = float(words[28]) * interval_scale
        cam[1][3][2] = FLAGS.max_d
        cam[1][3][3] = cam[1][3][0] + cam[1][3][1] * cam[1][3][2]
    elif len(words) == 30:
        cam[1][3][0] = words[27]
        cam[1][3][1] = float(words[28]) * interval_scale
        cam[1][3][2] = words[29]
        cam[1][3][3] = cam[1][3][0] + cam[1][3][1] * cam[1][3][2]
    elif len(words) == 31:
        cam[1][3][0] = words[27]
        cam[1][3][1] = float(words[28]) * interval_scale
        cam[1][3][2] = words[29]
        cam[1][3][3] = words[30]
    else:
        cam[1][3][0] = 0
        cam[1][3][1] = 0
        cam[1][3][2] = 0
        cam[1][3][3] = 0

    return cam
