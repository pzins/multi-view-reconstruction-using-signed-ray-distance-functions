#!/bin/bash
optim_script=./src/main.py
rescale=1
config="./configs/conf_blendedmvs.yaml"

# scene_51
subject="scene_51"
for i in $(seq 0 35)
do
    python $optim_script -s $subject --rescale $rescale --cam_group $i -c $config --transfo_fp32
done

# scene_99
# subject="scene_99"
# for i in $(seq 0 13)
# do
#     python $optim_script -s $subject --rescale $rescale --cam_group $i -c ./configs/conf_blendedmvs.yaml --transfo_fp32
# done