
import argparse
import os
import time
from multiprocessing import Pool

import numpy as np
import trimesh
from scipy import sparse
from skimage.transform import rescale

from utils import *

parser = argparse.ArgumentParser(description='Create depthmaps')
parser.add_argument('-d', '--dataset', action='store', required=True, type=str, help='Dataset path')
parser.add_argument('-s', '--subject', action='store', required=True, type=str, help='Subject')
parser.add_argument('-m', '--mesh_type', action='store', default="mesh_gt", type=str, help='Mesh type: [mesh_gt, mesh_mvpifu]')
parser.add_argument('-o', '--output_folder', action='store', default="depthmaps_gt", type=str, help='Output folder')
parser.add_argument('--width', action='store', default=-1, type=int, help='Resolution width')
parser.add_argument('--height', action='store', default=-1, type=int, help='Resolution height')
parser.add_argument('--rescale', action='store', default=1, type=float, help='Rescale')
parser.add_argument('--override', action='store_true', help='Override if exist')
parser.add_argument('--sparse', action='store_true', help='Save sparse')
parser.add_argument('--mask', action='store_true', help='Use masks to delete/create surface')
parser.add_argument('-dt', '--data_type', action='store', default="RP", type=str, help='[RP, DT, Kinovis]')
args = parser.parse_args()

# Cameras
if args.data_type == "DTU":
    if int(args.subject[4:]) < 82:
        cams = list(range(1,50))
    else:
        cams = list(range(1,65))
elif args.data_type == "Kinovis":
    cams = list(range(0,65))
elif args.data_type == "RP":
    cams = list(range(342,360,2)) + list(range(0,20,2))
elif args.data_type == "BlendedMVS":
    res = get_cameras_cluster(args.dataset, args.subject)
    cams = list(res.keys())


out_folder = os.path.join(args.dataset, args.output_folder, args.subject)
os.makedirs(out_folder, exist_ok=True)


if not args.override and os.path.exists(out_folder) and len(os.listdir(out_folder)) > 0:
    print("Skip", args.subject)
    exit(0)

rescale =  args.rescale if args.rescale != 1 else None
data = load_data(args.data_type, args.dataset, args.subject, cams, mesh_folder=args.mesh_type, load_image=False, rescale=rescale)
mesh = data["mesh"]
calib_tensor = data["calib"]
proj_tensor = data["proj"]
mask_tensor = data["mask"]


if args.width == -1:
    args.width = mask_tensor.shape[2]
if args.height == -1:
    args.height = mask_tensor.shape[1]

cam_centers = np.linalg.inv(calib_tensor)[:,:,3]


def compute_depthmap(cam):
    try:
        coo = get_pixels(0, args.height, 0, args.width, 1)
        coo[:,:2] += 0.5

        origins, directions = ray_directions_and_origins(coo, proj_tensor, calib_tensor, cam, cam_centers, w=args.height, h=args.width, is_opengl=(args.data_type=="RP"))
        pixels = coo[:,:2].astype(int)

        points, index_ray, index_tri = mesh.ray.intersects_location(origins, directions, multiple_hits=False)
        # ==================== Compute distance / depth ====================
        # For each hit, find the distance along its vector
        distance = trimesh.util.diagonal_dot(points - origins[0], directions[index_ray])
        depth = distance # Not really depth map but distance map

        # find pixel locations of actual hits
        pixel_ray = pixels[index_ray]

        # Depthmaps
        depthmap = np.zeros((args.height, args.width))
        depthmap[pixel_ray[:, 1], pixel_ray[:, 0]] = depth
        depthmap[depthmap==0] = DEPTH_NO_SURFACE
        # depthmap[row, col]

        # Normals
        normals = np.zeros((args.height, args.width, 3))
        normals[pixel_ray[:, 1], pixel_ray[:, 0]] = mesh.face_normals[index_tri]
        normals[normals==0] = DEPTH_NO_SURFACE
        # normals[row, col]

        depthmap = depthmap.astype(np.float32)

        if args.sparse:
            depthmap = sparse.csr_matrix(depthmap)
            sparse.save_npz(os.path.join(out_folder, str(cams[cam]) + "_dist.npz"), depthmap)
            # sparse.save_npz(os.path.join(out_folder, str(cams[cam]) + "_depth.npz"), depthmap)
        else:
            np.save(os.path.join(out_folder, str(cams[cam]) + "_dist.npy"), depthmap)
        
    except:
        print("Error", cam)


# Multi-processing
start_time = time.time()
num_cpu = 10
pool = Pool(num_cpu)
pool.map(compute_depthmap, list(range(len(cams))))

print(args.subject, "done in %.2fs" % (time.time()-start_time))