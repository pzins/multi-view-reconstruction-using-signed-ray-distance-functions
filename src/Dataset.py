import os

import numpy as np
import torch
import torchvision.transforms as transforms
from scipy import sparse
from torch.utils.data import Dataset
from tqdm import tqdm

from utils import *


class MVDataset(Dataset):

    def __init__(self, cfg):
        print("Creating dataset ...")
        self.cfg = cfg
        self.width = None
        self.height = None

        self.to_tensor = transforms.Compose([
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
        
        data = load_data(self.cfg["data_type"], self.cfg["dataset"], self.cfg["subject"], self.cfg["cams"], load_mesh=False, rescale=self.cfg["rescale"], erode_mask=self.cfg["erode_mask"])
    
        self.calib_tensor_np = data["calib"]
        
        self.proj_tensor_np = data["proj"]
        
        self.cam_center_tensor_np = np.linalg.inv(self.calib_tensor_np)[:,:,3]

        self.mask_tensor = torch.from_numpy(data["mask"])
        
        self.render_list = []
        self.render_list.append(torch.from_numpy(data["image"]))
        render_tensor = torch.stack(self.render_list)
        if self.cfg["norm_images"]:
            render_tensor = render_tensor.reshape(
                render_tensor.shape[0] * render_tensor.shape[1],
                render_tensor.shape[2],
                render_tensor.shape[3],
                render_tensor.shape[4],
            )
            render_tensor = render_tensor.permute(0,3,1,2)
            render_tensor = self.to_tensor(render_tensor)
            render_tensor = render_tensor.permute(0,2,3,1)
            render_tensor = render_tensor.view(
                1,
                -1,
                render_tensor.shape[1],
                render_tensor.shape[2],
                render_tensor.shape[3],
            )
        self.render_tensor = render_tensor[0]


    def __len__(self):
        return len(self.cfg["subject"])


    def get_depthmaps(self, subject, depthmaps_path, it=""):
        depthmaps = []
        for cam in range(len(self.cfg["cams"])):
            path = os.path.join(depthmaps_path, subject, it, str(self.cfg["cams"][cam]) + "_dist.npy")
            if os.path.isfile(path):
                d = np.load(path)
            else:
                d = sparse.load_npz(path.replace("npy", "npz"))
                d = np.array(d.todense())

            depthmaps.append(d)

        depthmaps = np.stack(depthmaps)
        depthmaps = depthmaps.astype(np.float32)
        
        mask_depthmaps_tensor = torch.from_numpy(depthmaps < self.cfg["DEPTH_NO_SURFACE"])
        depthmaps = torch.from_numpy(depthmaps)
        
        self.depthmaps = depthmaps
        self.mask_depthmaps = mask_depthmaps_tensor
        
        if self.width is None:
            self.width = depthmaps.shape[2]
        if self.height is None:
            self.height = depthmaps.shape[1]

        return depthmaps, mask_depthmaps_tensor




    def get_item(self, index):
        res = {}
        
        if self.cfg["depthmaps_path"] is not None:
            depthmaps, mask_depthmaps = self.get_depthmaps(self.cfg["subject"], self.cfg["depthmaps_path"], self.cfg["it"])
            tmp = {
                "depthmaps": depthmaps,
                "mask_depthmaps": mask_depthmaps,
            }
            res.update(tmp)
            
        res.update({
            "calib": torch.from_numpy(self.calib_tensor_np),
            "proj": torch.from_numpy(self.proj_tensor_np),
            "render": self.render_tensor,
            "mask": self.mask_tensor,
            "cam_center": torch.from_numpy(self.cam_center_tensor_np),
        })

        return res



    def __getitem__(self, index):
        return self.get_item(index)

