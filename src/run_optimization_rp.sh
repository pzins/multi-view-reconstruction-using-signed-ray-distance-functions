#!/bin/bash
optim_script=./src/main.py
rescale=1
config="./configs/conf_rp.yaml"
subject="rp_dennis_posed_004"

python $optim_script -s $subject --rescale $rescale --cam_group 1 -c $config
python $optim_script -s $subject --rescale $rescale --cam_group 2 -c $config
