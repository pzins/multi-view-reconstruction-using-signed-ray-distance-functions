import os

from utils import *


def get_dtu_cameras(config):
    allgroups = [
        [1,2,10,11,12,13,14],
        [3,4,5,6,7,8,19],
        [9,15,16,17,18,23,24],
        [20,21,22,37,38,39,40],
        [34,35,36,41,42,43,44],
        [25,26,32,33,45,46,47],
        [27,28,29,30,31,48,49],
        [50,51,58,59,60,61,62],
        [52,53,54,55,56,57,63,64]
    ]
    cams = allgroups[config["cam_group"] - 1]
    out_file = os.path.join(config["dataset"], config["out"], config["subject"], "9999", str(cams[0])+".npy")
    if os.path.isfile(out_file) or os.path.isfile(out_file.replace(".npy", "_dist.npy")):
        print("Already exist", cams)
        exit(0)
    return cams


def get_kinovis_cameras(config):
    pair_file = "./data/dataset_kinovis/pair/%s/pair.txt" % config["subject"]

    # cluster = get_cameras_cluster("", "", pair_file, 20)
    cluster = get_cameras_cluster("", "", pair_file, 10)
    # cams = cluster[config["cam_group"]][:4]
    cams = cluster[config["cam_group"]][:6]

    cams = [config["cam_group"]] + cams

    out_file = os.path.join(config["dataset"], config["out"], config["subject"], "99990", str(cams[0])+".npy")
    if os.path.isfile(out_file) or os.path.isfile(out_file.replace(".npy", "_dist.npy")):
        print("Already exist", cams)
        exit(0)

    return cams


def get_rp_cameras(config):
    if config["cam_group"] == 1:
        cams = list(range(0,20,2))
    elif config["cam_group"] == 2:
        cams = list(range(342,360,2))
    return cams


def get_blendedmvs_cameras(config):
    cluster = get_cameras_cluster(config["dataset"], config["subject"])
    all_cams = list(cluster.keys())
    
    all_calib = load_data(config["data_type"], config["dataset"], config["subject"], all_cams, load_mesh=False, load_image=False, load_mask=False)["calib"]
    # cams = filter_blendedmvs_cluster(config["cam_group"], cluster, all_calib, 6) # filter with camera orientation compared to ref_camera
    cams = filter_blendedmvs_cluster(config["cam_group"], cluster, all_calib, 4) # filter with camera orientation compared to ref_camera
    
    cams = [config["cam_group"]] + cams
    
    out_file = os.path.join(config["dataset"], config["out"], config["subject"], "99990", str(cams[0])+".npy")
    if os.path.isfile(out_file) or os.path.isfile(out_file):
        print("Already exist", cams)
        exit(0)

    return cams
