import os

import cv2
import numpy as np
import trimesh
from PIL import Image
from scipy import ndimage
from scipy.io import loadmat


def load_data_DTU(base_path, subject, cams, mesh_folder="mesh_gt", load_mesh=True, load_image=True, load_mask=True, load_calib=True, rescale=None, erode_mask=-1, dilate_mask=-1, random_lights=False):
    image_list = []
    image_orig_list = []
    mask_list = []
    extr_list = []
    intr_list = []
    img_suffix = '_max'
    if random_lights:
        suffix = ["_0_r5000", "_1_r5000", "_3_r5000", "_4_r5000", "_5_r5000", "_6_r5000", "_6_r5000", "_max", "_max", "_max", "_max"]
        img_suffix = suffix[np.random.randint(0,len(suffix))]
    
    for cam in cams:
        param_path = os.path.join(base_path, "calib", subject, 'pos_%03d.txt' % cam)
        render_path = os.path.join(base_path, "images", subject, 'rect_%03d' % cam + img_suffix + ".png")
        mask_path = os.path.join(base_path, "masks", subject, '%03d.png' % cam)
        
        if load_image:
            render = Image.open(render_path).convert('RGB')
            
            if rescale is not None:
                new_w = int(render.size[0] * rescale)
                new_h = int(render.size[1] * rescale)
                render = render.resize((new_w, new_h), Image.ANTIALIAS)
            arr = np.array(render) / 255.0
            image_list.append(arr)

        if load_calib:
            calib = np.loadtxt(param_path)
            K, Rt = load_K_Rt_from_P(calib)
            if rescale is not None:
                K[0,0] *= rescale
                K[1,1] *= rescale
                K[0,2] *= rescale
                K[1,2] *= rescale
            
            extr_list.append(np.linalg.inv(Rt))
            intr_list.append(K)

        if load_mask:
            if not os.path.isfile(mask_path):
                new_w = 1600
                new_h = 1200
                if rescale is not None:
                    new_w = int(new_w * rescale)
                    new_h = int(new_h * rescale)
                mask_list.append(np.ones((new_h,new_w)) / 255.0)
            else:
                mask = Image.open(mask_path).convert('L')
                if rescale is not None:
                    new_w = int(mask.size[0] * rescale)
                    new_h = int(mask.size[1] * rescale)
                    mask = mask.resize((new_w, new_h), Image.ANTIALIAS)
                
                mask = np.array(mask) / 255.0
                mask[mask > 0] = 1.0
                
                if erode_mask > 0:
                    mask = ndimage.binary_erosion(mask, iterations=erode_mask)
                elif dilate_mask > 0:
                    mask = ndimage.binary_dilation(mask, iterations=dilate_mask)
                mask_list.append(mask)
    res = {}

    # Load Plane and Observation masks
    plane_id = int(subject[4:])
    obs_mask_file = loadmat(os.path.join(base_path, "ObsMask","ObsMask%d_10.mat" % plane_id))
    ObsMask, BB, Res = [obs_mask_file[attr] for attr in ['ObsMask', 'BB', 'Res']]
    res["ObsMask"] = ObsMask
    res["BB"] = BB.astype(np.float32)
    res["Res"] = Res
    
    plane_file = os.path.join(base_path, "ObsMask", "Plane%d.mat" % plane_id)
    
    if not os.path.isfile(plane_file):
        assoc = {
            7:9,8:9,20:19,26:25,27:25,52:51,53:51,54:51,56:55,57:55,58:55,66:65,67:65,68:65,70:69,71:69,72:69,73:74,
            82:83,85:69,86:69,87:69,88:69,89:69,90:69,91:69,92:69,101:100,104:100,107:106,108:106,109:106,111:110,
            112:110,113:110,115:114,116:114,117:114,119:118,120:118,121:118,123:122,124:122,125:122
        }
        plane_file = os.path.join(base_path, "ObsMask", "Plane%d.mat" % assoc[plane_id])

    ground_plane = loadmat(plane_file)['P']
    res["plane"] = ground_plane
    
    if load_mesh:
        mesh = trimesh.load(os.path.join(base_path, mesh_folder, subject, "poisson_%03d_trimmed.ply" % int(subject[4:])))
        res["mesh"] = mesh

    if load_image:
        render_tensor = np.stack(image_list, 0).astype(np.float32)
        res["image"] = render_tensor
        if len(image_orig_list) > 0:
            render_orig_tensor = np.stack(image_orig_list, 0).astype(np.float32)
            res["image_orig"] = render_orig_tensor
    
    if load_mask:
        mask_tensor = np.stack(mask_list, 0).astype(np.float32)
        res["mask"] = mask_tensor

    if load_calib:
        extr_tensor = np.stack(extr_list, 0).astype(np.float32)
        intr_tensor = np.stack(intr_list, 0).astype(np.float32)
        res["proj"] = intr_tensor
        res["calib"] = extr_tensor
    
    return res

def load_K_Rt_from_P(P):
    out = cv2.decomposeProjectionMatrix(P)
    K = out[0]
    R = out[1]
    t = out[2]

    K = K/K[2,2]
    intrinsics = np.eye(4)
    intrinsics[:3, :3] = K

    pose = np.eye(4, dtype=np.float32)
    pose[:3, :3] = R.transpose()
    pose[:3,3] = (t[:3] / t[3])[:,0]

    return intrinsics, pose
