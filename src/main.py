import argparse
import os
import shutil

import torch
import torch.nn as nn
import yaml
from tqdm import tqdm

from cameras import *
from Dataset import *
from photoconsistency import *
from PhotoNet import *
from utils import *

if "charmeleon" not in socket.gethostname() and "Jynx" not in socket.gethostname() and "lapras" not in socket.gethostname() and "geyser" not in socket.gethostname():
    print("Set cache folder in env")
    os.environ["PYTORCH_KERNEL_CACHE_PATH"] = "/services/morpheo/data/pzins/pytorch_kernel_cache"

# =================== CONFIGURATION ===================
parser = argparse.ArgumentParser(description='Optimize depthmaps with photoconsistency')
parser.add_argument('-d', '--dataset', action='store', required=False, type=str, help='Dataset folder')
parser.add_argument('-s', '--subject', action='store', required=True, type=str, help='Subject')
parser.add_argument('-c', '--config', action='store', required=True, type=str, help='Config file')
parser.add_argument('--checkpoint', action='store', required=False, type=str, help='Load checkpoint')
parser.add_argument('--depthmaps', action='store', type=str, help='Depthmaps folder')
parser.add_argument('--it', action='store', type=str, help='Iteration if optimized depthmaps')
parser.add_argument('--normals', action='store_true', help='Use a consistency loss on normals')
parser.add_argument('--with_tqdm', action='store_true', help='Use tqdm over the cameras')
parser.add_argument('--no_accumulate', action='store_true', help='Do not accumulate gradients over all the rays of all the cameras')
parser.add_argument('--rescale', action='store', type=float, help='Rescale')
parser.add_argument('-transfo_fp32', '--transfo_fp32', action='store_true', help='Do not use float16 for the transfromer')
parser.add_argument('-o', '--out', action='store', type=str, help='Output folder')
parser.add_argument('-dt', '--data_type', action='store', type=str, help='[RP, DTU, Kinovis]')
parser.add_argument('-cam_group', '--cam_group', action='store', default=1, type=int, help='Cameras group')
args = parser.parse_args()


print("==========", args.subject, "==========")

with open(args.config, "r") as f:
    config = yaml.safe_load(f)

# Overwrite config with command line arguments
config["subject"] = args.subject
config["cam_group"] = args.cam_group
if args.dataset is not None:
    config["dataset"] = args.dataset
if args.data_type is not None:
    config["data_type"] = args.data_type
if args.rescale is not None:
    config["rescale"] = args.rescale
if args.depthmaps is not None:
    config["depthmaps"] = args.depthmaps
if args.it is not None:
    config["it"] = args.it
if args.out is not None:
    config["out"] = args.out
if args.checkpoint is not None:
    config["checkpoint"] = args.checkpoint
if args.no_accumulate == True:
    config["accumulate_grad"] = False


out_folder = os.path.join(config["dataset"], config["out"], args.subject, "9999") # save depthmaps for all cameras of the group
out_folder_main = os.path.join(config["dataset"], config["out"], args.subject, "99990") # save only the depthmap of the main camera in the group
depthmaps_png_folder = os.path.join(config["dataset"], config["out"], config["subject"])

show_depthmaps_freq = config["show_depthmaps_freq"]
save_depthmaps_freq = config["save_depthmaps_freq"]
stages = config["stages"]


use_cuda = True
device = torch.device("cuda" if use_cuda else "cpu")

# Get cameras
if config["data_type"] == "DTU":
    cams = get_dtu_cameras(config)
elif config["data_type"] == "Kinovis":
    cams = get_kinovis_cameras(config)
elif config["data_type"] == "RP":
    cams = get_rp_cameras(config)
elif config["data_type"] == "BlendedMVS":
    cams = get_blendedmvs_cameras(config)
else:
    print("Unknown group")
    exit(0)

print("Camera group", args.cam_group, ":", cams)

if args.normals:
    normal_loss = nn.L1Loss()

size_batch = 10000
size_batch_photo = 512*8
size_batch_photo = 512 * 4
size_batch_photo = 512

num_epochs = config["num_epochs"]


# Create output folders
for cam in cams:
    os.makedirs(os.path.join(depthmaps_png_folder, "depthmaps_cam_" + str(cam)), exist_ok=True)
os.makedirs(os.path.join(config["dataset"], config["out"]), exist_ok=True)
os.makedirs(out_folder, exist_ok=True)
os.makedirs(out_folder_main, exist_ok=True)


# Configure the NN
if config["prior"] == "learned":
    model = PhotoNet.load_from_checkpoint(config["checkpoint"], strict=True).to(device).half()

    # Use float32 for the transformer encoder to avoid nan
    if args.transfo_fp32:
        model.transfo_fp32 = True
        if model.transformer_encoder is not None:
            model.transformer_encoder = model.transformer_encoder.float()

    # Enable/Disable pooling opeation depending on the scaling
    if config["rescale"] == 1:
        model.pool_1 = True
        model.pool_2 = True
        if config["data_type"] in ["BlendedMVS", "Kinovis"]:
            model.pool_1 = False
            model.pool_2 = False

    elif config["rescale"] == 0.5:
        model.pool_1 = False
        model.pool_2 = True
    elif config["rescale"] == 0.25:
        model.pool_1 = False
        model.pool_2 = False
    else:
        print("Unkown rescale")
        exit(0)

    model.eval()


# Configuration of the dataset
config_dataset = {
    "cams": cams,
    "dataset": config["dataset"],
    "depthmaps_path": os.path.join(config["dataset"], config["depthmaps"]),
    "it": config["it"],
    "DEPTH_NO_SURFACE": DEPTH_NO_SURFACE,
    "subject": args.subject,
    "data_type": config["data_type"],
    "rescale": config["rescale"],
    # "erode_mask": 20,
    "erode_mask": -1,
    "norm_images": (config["prior"]!="median" and config["prior"]!="NCC"),
}

dataset = MVDataset(config_dataset)
data = dataset[0]


# =================== GET DATA ===================
calib_tensor = data["calib"].to(device)
proj_tensor = data["proj"].to(device)
cam_centers = data["cam_center"].to(device)
render_tensor = data["render"].to(device)
if "render_orig" in data:
    render_orig_tensor = data["render_orig"].to(device)
else:
    render_orig_tensor = None
depthmaps = data["depthmaps"].to(device)
depthmaps.requires_grad = True
mask_tensor = data["mask"].to(device)

if config["data_type"] == "RP":
    K = get_K_from_proj(data["proj"], h=render_tensor.shape[1], w=render_tensor.shape[1])
    K = torch.from_numpy(K)
else:
    K = data["proj"]

if "daisy" in data:
    daisy_tensor = data["daisy"].to(device)

tolerance = 0.5 # tolerance factor for the depths that don't cross any surface
mask_depthmaps_tensor = torch.from_numpy(data["depthmaps"].numpy() < DEPTH_NO_SURFACE*tolerance).to(device)

calib_inv = torch.linalg.inv(calib_tensor)
K = K.to(device).float()
K_inv = torch.linalg.inv(K[:,:3,:3]) 
# ================================================


# Initial lr is not important, it's overwritten by the configuration
optimizer = torch.optim.Adam([depthmaps], lr=0)


if config["prior"] == "learned" and model.window == 0:
    with torch.no_grad():
        features_list = model.filter(render_tensor.half())
else:
    features_list = None


samples_all_cams = []
photo_all_cams = []

print("Start optimization")
epoch = 0
while epoch < num_epochs:
    losses = []

    recompute_samples = False
    recompute_photo = False

    if epoch in stages:
        print("\nNew stage:", stages[epoch], "\n")

        if "std_c" in stages[epoch]:
            std_c = stages[epoch]["std_c"]
        if "std_d" in stages[epoch]:
            std_d = stages[epoch]["std_d"]
        if "recompute_photo" in stages[epoch]:
            recompute_photo = stages[epoch]["recompute_photo"]
        if "recompute_samples" in stages[epoch]:
            recompute_samples = stages[epoch]["recompute_samples"]
        if "offset" in stages[epoch]:
            offset = stages[epoch]["offset"]
        if "num_samples" in stages[epoch]:
            num_samples = stages[epoch]["num_samples"]
            if config["data_type"] == "DTU":
                num_samples += (num_samples-1)
        if "lr" in stages[epoch]:
            for g in optimizer.param_groups:
                g['lr'] = stages[epoch]["lr"]

    if recompute_samples:
        samples_all_cams = []
        total_samples = 0
        if num_samples == 1:
            scales = torch.tensor([0])[:,None,None].to(device)
        else:
            l = torch.linspace(-offset, 0, num_samples//2+1)
            r = torch.linspace(l[1]-l[0], offset, num_samples//2)
            scales = torch.cat([l, r]).to(device)
            scales = scales[:, None, None]
    
    if recompute_photo:
        photo_all_cams = []

    r = tqdm(range(len(cams))) if args.with_tqdm else range(len(cams))
    for cam in r:

        # =================== COMPUTE SAMPLES ===================
        if recompute_samples:
            pix = None
            
            samples_cam = compute_samples(cam, num_samples, depthmaps.shape[1], depthmaps.shape[2], depthmaps, calib_inv,
                                            K_inv, cam_centers, scales, device, tolerance, config["data_type"])

            samples_all_cams.append(samples_cam)
            total_samples += samples_cam.shape[0] * samples_cam.shape[1]

        else:
            samples_cam = samples_all_cams[cam]



        # =================== COMPUTE PHOTO ===================
        if recompute_photo:
            photo = torch.zeros((samples_cam.shape[0], samples_cam.shape[1], 1), device=device)
            for it in tqdm(range(samples_cam.shape[0] // size_batch_photo + 1)):
                samples = samples_cam[it*size_batch_photo:(it+1)*size_batch_photo]
                if samples.shape[0] == 0:
                    continue
                curr_num_rays = samples.shape[0]

                if config["prior"] == "learned":
                    with torch.no_grad():
                        p = photo_learned(samples, calib_tensor, calib_inv, proj_tensor, render_tensor, cam, model, features_list, std_c, curr_num_rays, config, False, it)
                
                elif config["prior"] == "median":
                    p = photo_median(samples, calib_tensor, proj_tensor, render_tensor, config, std_c)
                
                else:
                    print("Unknown prior:", config["prior"])
                    exit(0)
                
                if p.isnan().max() == 1:
                    print("Nan")
                    exit(0)

                photo[it*size_batch_photo:(it+1)*size_batch_photo] = p

            photo_all_cams.append(photo)
        
        else:
            photo = photo_all_cams[cam]


        for it in range(samples_cam.shape[0] // size_batch + 1):
            samples = samples_cam[it*size_batch:(it+1)*size_batch]
            p = photo[it*size_batch:(it+1)*size_batch]
            curr_num_rays = samples.shape[0]
            

            di = get_color_torch(
                    samples.reshape(samples.shape[0] * samples.shape[1], samples.shape[2]).T,
                    calib_tensor,
                    proj_tensor,
                    depthmaps[...,None], config["data_type"])[None,...]
            di = di.reshape(curr_num_rays, -1, len(cams), 1)
            
            
            zi = samples[:,:,None,:] - cam_centers[None,None,:,:]
            zi = zi[:,:,:,:3]
            zi = zi.reshape(zi.shape[0] * zi.shape[1] * zi.shape[2], zi.shape[3])
            zi = torch.linalg.norm(zi, dim=1)
            zi = zi.reshape(curr_num_rays, -1, len(cams), 1)

            srdf = (di - zi).squeeze(-1)
            

            srdf_exp = torch.exp(-srdf**2 / (2 * std_d**2))
            
            cost = p * torch.prod(srdf_exp + 1, dim=2, keepdim=True)
            
            cost = -torch.sum(cost)
            losses.append(cost.detach())

            cost.backward()

            # Zero gradients of the other cameras
            # binary_mask = torch.zeros((depthmaps.grad.shape[0],1,1)).to(device)
            # binary_mask[cam] = 1.0
            # depthmaps.grad *= binary_mask

            # torch.nn.utils.clip_grad_norm_(depthmaps, 10)
            
            if config["accumulate_grad"] == False:
                torch.nn.utils.clip_grad_norm_(depthmaps, 10)
                optimizer.step()
                optimizer.zero_grad()
        

       
    if config["accumulate_grad"] == True:
        optimizer.step()
        optimizer.zero_grad()
    
    # Save depthmaps
    if epoch % save_depthmaps_freq == 0:
        save_depthmaps(depthmaps.detach().cpu().numpy(), os.path.join(config["dataset"], config["out"], args.subject, str(epoch)), cams)
        save_depthmaps(depthmaps[0].detach().cpu().numpy(), os.path.join(config["dataset"], config["out"], args.subject, str(epoch), "main_cam"), cams)
        
    print("Epoch %d - loss %f" % (epoch, sum(losses)/total_samples))
    
    epoch += 1

    if epoch >= num_epochs:
        save_depthmaps(depthmaps.detach().cpu().numpy(), os.path.join(config["dataset"], config["out"], args.subject, str(epoch)), cams)


print("Copy last epoch into 9999/9990:", num_epochs)
in_path = os.path.join(config["dataset"], config["out"], args.subject, str(num_epochs))
for i in range(len(cams)):
    shutil.copy(os.path.join(in_path, str(cams[i]) + "_dist.npy"), os.path.join(out_folder, str(cams[i]) + "_dist.npy"))
    if i == 0:
        shutil.copy(os.path.join(in_path, str(cams[i]) + "_dist.npy"), os.path.join(out_folder_main, str(cams[i]) + "_dist.npy"))
        print("Copy ", os.path.join(in_path, str(cams[i]) + "_dist.npy"), "   to   ", os.path.join(out_folder_main, str(cams[i]) + "_dist.npy"))
print("Finished group", args.cam_group, "of scan", args.subject)
