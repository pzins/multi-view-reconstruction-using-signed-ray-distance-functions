# Code based on https://github.com/andyzeng/tsdf-fusion-python
"""Fuse 1000 RGB-D images from the 7-scenes dataset into a TSDF voxel volume with 2cm resolution.
"""
import os
import time

import cv2
import numpy as np

import fusion_rp as fusion

import argparse

def export_points(filename, points):
    # points : (N, 3/4)
    if points.shape[-1] == 4:
        points = points[:, :3]
    data = np.hstack((np.array(["v"] * points.shape[0]).reshape(-1,1), points))
    np.savetxt(filename, data,  fmt="%s %s %s %s", delimiter=" ")

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='TSDF Fusion')
	parser.add_argument('-i', '--input_folder', action='store', required=True, type=str, help='Input folder')
	parser.add_argument('-w', '--weights', action='store', type=str, help='Wiew weights file')
	parser.add_argument('-o', '--out_mesh', action='store', default="data/mesh.ply", type=str, help='Output mesh')
	args = parser.parse_args()

	n_imgs = 19
	# Debug
	img_idx = 0

	vol_bnds = np.array([
		[-40,40],
		[0,200],
		[-40,40]
	])
	print(vol_bnds)

	# ======================================================================================================== #
	# Integrate
	# ======================================================================================================== #
	# Initialize voxel volume
	print("Initializing voxel volume...")
	
	tsdf_vol = fusion.TSDFVolume(vol_bnds, voxel_size=0.30) # RP

	weight_im = None
	if args.weights is not None:
		weight_im = np.load(args.weights)

	# Loop through RGB-D images and fuse them together
	t0_elapse = time.time()
	for i in range(n_imgs):
		if n_imgs == 1:
			i = img_idx
		print("Fusing frame %d/%d"%(i+1, n_imgs))

		# Read RGB-D image and camera pose
		color_image = cv2.cvtColor(cv2.imread(args.input_folder + "/frame-%06d.color.jpg"%(i)), cv2.COLOR_BGR2RGB)
		depth_im = cv2.imread(args.input_folder + "/frame-%06d.depth.png"%(i),-1).astype(float)
		
		# Remove the factor x10 when preparing data, but keep accuracy
		depth_im /= 1000.  # depth is saved in 16-bit PNG in millimeters
		depth_im[depth_im == 65.535] = 0  # set invalid depth to 0 (specific to 7-scenes dataset)
		depth_im *= 100
		
		
		cam_pose = np.loadtxt(args.input_folder + "/frame-%06d.pose.txt"%(i))
		cam_intr = np.loadtxt(args.input_folder + "/frame-%06d.intrinsics.txt"%(i), delimiter=' ')

		# Integrate observation into voxel volume (assume color aligned with depth)
		# tsdf_vol.integrate(color_image, depth_im, cam_intr, cam_pose, obs_weight=w[i])
		if weight_im is None:
			weight_im = np.ones((n_imgs, depth_im.shape[0], depth_im.shape[1]))
		tsdf_vol.integrate(color_image, depth_im, weight_im[0], cam_intr, cam_pose, obs_weight=1.)
		# tsdf_vol.integrate(color_image, depth_im, weight_im[i], cam_intr, cam_pose, obs_weight=1.)

	fps = n_imgs / (time.time() - t0_elapse)
	print("Average FPS: {:.2f}".format(fps))

	# Get mesh from voxel volume and save to disk (can be viewed with Meshlab)
	print("Saving mesh to mesh.ply...")
	verts, faces, norms, colors = tsdf_vol.get_mesh()
	if "kinovis" in args.input_folder.lower():
		verts /= 100
	fusion.meshwrite(args.out_mesh, verts, faces, norms, colors)
	"""
	# Get point cloud from voxel volume and save to disk (can be viewed with Meshlab)
	print("Saving point cloud to pc.ply...")
	point_cloud = tsdf_vol.get_point_cloud()
	fusion.pcwrite("pc.ply", point_cloud)
	"""