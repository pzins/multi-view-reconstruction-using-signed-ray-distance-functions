import os

import numpy as np
import torch
import torchvision.transforms as transforms
from scipy import sparse
from sklearn.neighbors import NearestNeighbors
from torch.utils.data import Dataset

from utils import *


class PhotoDataset(Dataset):

    def __init__(self, cfg, phase="train"):
        print("Creating dataset ...")
        self.close_cams = True # use nearest neighbours to select cameras
        self.use_far_cameras = False # use camera from 50 to 64 (farther than the others)
        self.cfg = cfg
        self.phase = phase
        self.dataset = self.cfg["dataset"]
        self.DEPTH_NO_SURFACE = self.cfg["DEPTH_NO_SURFACE"]
        self.width = None
        self.height = None

        if self.phase == "train":
            self.subjects = self.cfg["subjects_tr"]
        elif self.phase == "val":
            self.subjects = self.cfg["subjects_val"]
        
        self.to_tensor = transforms.Compose([
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])

        if self.cfg["data_type"] == "BlendedMVS":
            self.all_calib = {}
            self.scene_cam_clusters = {}
            for subject in self.subjects:
                tmp = get_cameras_cluster(base_path=self.dataset, scene=subject)
                self.scene_cam_clusters[subject] = tmp
                all_cams = list(tmp.keys())
                self.all_calib[subject] = load_data(self.cfg["data_type"], self.dataset, subject, all_cams, load_mesh=False, load_image=False, load_mask=False)["calib"]

        if self.cfg["data_type"] == "RP":
            self.cam_rp_kinovis = get_cameras_cluster("", "", pair_file=os.path.join(self.dataset, 'pair.txt'))

        if self.close_cams:
            self.all_cam_centers = np.load("./configs/cam_centers.npy").T

        self.cam_neighbours = {}
        idx = np.arange(self.all_cam_centers.shape[0])
        if not self.use_far_cameras or int(subject[4:]) < 82:
            idx = idx[:49]
            self.all_cam_centers = self.all_cam_centers[:49]
        
        nn_cams = 11
        for i in idx:
            nbrs = NearestNeighbors(n_neighbors=nn_cams, algorithm='auto').fit(self.all_cam_centers)
            # Get the nn_cams closest neigbours
            id = nbrs.kneighbors(self.all_cam_centers[i:i+1,:], nn_cams, return_distance=False)
            id = id[0, 1:]
            self.cam_neighbours[i+1] = np.array(id)+1
           

        print("Create a PhotoDataset of size", len(self))


    def __len__(self):
        return len(self.subjects)


    def get_images_calib(self, subject):
        data = load_data(self.cfg["data_type"], self.dataset, subject, self.cams, load_mesh=False, rescale=self.cfg["rescale"], erode_mask=False, random_lights=self.cfg["random_lights"])
        calib_tensor = data["calib"]
        render_tensor = torch.from_numpy(data["image"])
        
        render_tensor = render_tensor.permute(0,3,1,2)
        render_tensor = self.to_tensor(render_tensor)
        render_tensor = render_tensor.permute(0,2,3,1)

        mask_tensor = torch.from_numpy(data["mask"])
        self.masks = mask_tensor

        self.K = data["proj"]
        self.calib = data["calib"]
        self.cam_centers = np.linalg.inv(calib_tensor)[:,:,3]

        res = {
            "calib": data["calib"],
            "proj": data["proj"],
            "cam_center": self.cam_centers,
            "render": render_tensor,
            "mask": mask_tensor
        }

        if self.cfg["data_type"] == "DTU":
            self.plane = torch.from_numpy(data["plane"])
            self.Res = torch.from_numpy(data["Res"])
            self.ObsMask = torch.from_numpy(data["ObsMask"])
            self.BB = torch.from_numpy(data["BB"])
        
        return res


    def get_depthmaps(self, subject, depthmaps_path, it=""):
        depthmaps = []
        for cam in range(len(self.cams)):
            path = os.path.join(depthmaps_path, subject, it, str(self.cams[cam]) + "_dist.npy")
            if os.path.isfile(path):
                d = np.load(path)
            else:
                d = sparse.load_npz(path.replace("npy", "npz"))
                d = np.array(d.todense())

            depthmaps.append(d)

        depthmaps = np.stack(depthmaps)
        depthmaps = depthmaps.astype(np.float32)
        
        mask_depthmaps_tensor = torch.from_numpy(depthmaps < self.DEPTH_NO_SURFACE)
        depthmaps = torch.from_numpy(depthmaps)
        
        if self.width is None:
            self.width = depthmaps.shape[2]
        if self.height is None:
            self.height = depthmaps.shape[1]

        return depthmaps, mask_depthmaps_tensor


    def compute_samples(self, subject, num_samples, resX, resY, depthmaps, calib, K, cam_centers, masks, tolerance=0.5, cams=None):
        samples_cam_pos = []
        samples_cam_neg = []
        
        if self.cfg["data_type"] == "RP":
            mesh_name = subject + "_100k.obj"
        elif self.cfg["data_type"] == "DTU":
            mesh_name = "poisson_" + subject[4:] + "_trimmed.ply"
        
        for cam in range(len(cams)):
            calib_inv = torch.from_numpy(np.linalg.inv(calib))
            if self.cfg["data_type"] == "RP":
                K_ok = get_K_from_proj(K, h=resY, w=resX)
            else:
                K_ok = K
            K_inv = torch.from_numpy(np.linalg.inv(K_ok[:,:3,:3])).float()

            # Get all foreground pixels
            pix = get_pixels_torch("cpu", 0, resX, 0, resY, indexing='ij').float()
            pix[:,:2] += 0.5
            mask = (depthmaps[cam] < DEPTH_NO_SURFACE * tolerance)
            mask = mask * self.masks[cam].bool()
            mask = mask.view(-1)
            pix = pix[mask]
            depth = depthmaps[cam].view(-1)[mask]
            random_idx = np.random.randint(0,pix.shape[0], self.cfg["num_rays"]*5)
            pix = pix[random_idx]
            depth = depth[random_idx]
            unprojectedPoint = K_inv[cam] @ pix.T

            unprojectedPoint = torch.cat([unprojectedPoint, torch.ones((1, unprojectedPoint.shape[1]))], dim=0)
            world = calib_inv[cam] @ unprojectedPoint

            directionVector = world - cam_centers[cam:cam+1].T
            directionVector = directionVector / torch.linalg.norm(directionVector[:3,:], axis=0, keepdim=True)
            directionVector = directionVector[:3,:]
            direction = directionVector[None, :, :].repeat(num_samples, 1, 1)

            if self.cfg["data_type"] == "RP":
                z_axis = torch.tensor([0,0,-1]).float()
            else:
                z_axis = torch.tensor([0,0,1]).float()
            camera_axis = calib_inv[cam,:3,:3] @ z_axis
            depth = (camera_axis[None,:] @ directionVector) * depth
            unprojectedPoint[:3,:] *= depth
            world = calib_inv[cam] @ unprojectedPoint
            if self.cfg["data_type"] == "DTU":
                # Mask using Observation mask
                patch = 60
                w = world[:3,:].T
                inbound = ((w >= self.BB[:1]-patch) & (w < self.BB[1:]+patch*2)).sum(axis=-1) ==3

                data_in = w[inbound]
                direction = direction[:,:,inbound]
                depth = depth[:,inbound]
                world = world[:,inbound]

                data_grid = np.around((data_in - self.BB[:1]) / self.Res)
                grid_inbound = ((data_grid >= 0) & (data_grid < torch.tensor(self.ObsMask.shape).unsqueeze(0))).sum(axis=-1) ==3

                data_grid_in = data_grid[grid_inbound].to(int)
                in_obs = self.ObsMask[data_grid_in[:,0], data_grid_in[:,1], data_grid_in[:,2]].bool()
                
                w = data_in[grid_inbound][in_obs]
                direction = direction[:,:,grid_inbound][:,:,in_obs]
                depth = depth[:,grid_inbound][:,in_obs]
                world = torch.vstack((w.T, torch.ones(1,w.shape[0])))

                # Mask using the plane
                masque = (self.plane * world).sum(0) > 0

                world = world[:,masque]
                depth = depth[:,masque]
                direction = direction[:,:,masque]
            
            elif self.cfg["data_type"] == "BlendedMVS":
                masque = (world[2,:] > -0.23) * (world[2,:] < 0.33)
                masque *= (world[0,:] < 0.23) * (world[0,:] > -0.33)
                world = world[:,masque]
                depth = depth[:,masque]
                direction = direction[:,:,masque]
                
            world = world[:,:self.cfg["num_rays"]]
            depth = depth[:,:self.cfg["num_rays"]]
            direction = direction[:,:,:self.cfg["num_rays"]]

            rnd_off = self.cfg["offset_low"] + np.random.rand(direction.shape[2]) * (self.cfg["offset_high"] - self.cfg["offset_low"])
            rnd_sign = (2*np.random.randint(0,2,size=(rnd_off.shape))-1)
            rnd_off *= rnd_sign

            scales = torch.from_numpy(np.array([np.zeros_like(rnd_off), rnd_off])).float()
            scales = scales[:, None]

            # compute samples with -offset / +offset
            points = world[None,:3,:] + scales * direction
            points = torch.cat((points, torch.ones((points.shape[0],1,points.shape[2]))), axis=1)
            points = points.permute(0,2,1)

            subsample_factor = 1
            points = points[:,::subsample_factor,:]

            
            if self.cfg["window"] > 0:
                
                limit = 10
                nb_points = points.shape[1]
                points = points.reshape(points.shape[0] * points.shape[1], -1).T
                points = torch.from_numpy(calib)[cam] @ points
                points = points.T
                points = points.reshape(2, points.shape[0]//2, -1)
                points = torch.repeat_interleave(points, (limit * 2 + 1)**2, 1)


                dist = 10
                offset = get_pixels_torch("cpu", -limit, limit+1, -limit, limit+1, 1)[:,:3]
                offset[:,2] = 0
                offset /= limit
                offset *= dist
                offset = offset.repeat(nb_points, 1)
                offset = offset[None, :, :]
                points[:,:,:3] = points[:,:,:3] + offset
                points = points.reshape(points.shape[0] * points.shape[1], -1).T
                points = torch.from_numpy(np.linalg.inv(calib[cam])) @ points
                points = points.T
                points = points.reshape(2, points.shape[0]//2, -1)

            samples_cam_pos.append(points[0, :, :])
            samples_cam_neg.append(points[1, :, :])
        
        samples_pos = torch.cat(samples_cam_pos).reshape(-1,4)
        samples_neg = torch.cat(samples_cam_neg).reshape(-1,4)

        return samples_pos, samples_neg



    def get_item(self, index):
        sid = index % len(self.subjects)
        subject = self.subjects[sid]
        nb_cams = np.random.randint(3, 10)
        nb_cams = 4

        # Choose close cameras
        if self.cfg["data_type"] == "DTU":
            if not self.close_cams:
                max_cam = 64
                if int(subject[4:]) < 82:
                    max_cam = 49
                self.cams = np.random.choice(np.arange(1, max_cam+1), nb_cams, replace=False)
            
            else:
                # Kmeans
                """
                idx = np.arange(self.all_cam_centers.shape[0])
                if not self.use_far_cameras or int(subject[4:]) < 82:
                    idx = idx[:49]
                
                # shuffle idx and apply to all_cam_centers
                np.random.shuffle(idx)
                all_cam_centers = self.all_cam_centers[idx,:]
                
                # define a neighbours area of size twice (3 times now) the number of cameras and maximum 15
                nn_cams = nb_cams*3
                if nn_cams > 15:
                    nn_cams = 15

                # Fit NN on all camera except the one that correspond to the first index
                nbrs = NearestNeighbors(n_neighbors=nn_cams, algorithm='auto').fit(all_cam_centers[1:,:])
                # Get the nn_cams closest neigbours
                id = nbrs.kneighbors(all_cam_centers[0:1,:], nn_cams, return_distance=False)
                nn_idx = idx[id[0]+1]+1
                
                # shuffle the nn_cams closest neighbours and keep only the correct amount
                np.random.shuffle(nn_idx)
                nn_idx = nn_idx[:nb_cams]
                
                # Concatenate original camera query
                self.cams = np.hstack((idx[0]+1, nn_idx))
                """

                # Cam neighbours
                idx = np.random.randint(1,50)
                c = self.cam_neighbours[idx]
                np.random.shuffle(c)
                c = c[:nb_cams]
                self.cams = np.concatenate(([idx], c))

                # Fixed cam groups
                """
                allgroups = [
                    [1,2,10,11,12,13,14],
                    [3,4,5,6,7,8,19],
                    [9,15,16,17,18,23,24],
                    [20,21,22,37,38,39,40],
                    [34,35,36,41,42,43,44],
                    [25,26,32,33,45,46,47],
                    [27,28,29,30,31,48,49],
                    # [50,51,58,59,60,61,62],
                    # [52,53,54,55,56,57,63,64]
                ]
                id = np.random.randint(len(allgroups))
                self.cams = np.array(allgroups[id])
                """

        elif self.cfg["data_type"] == "RP":
            cams = np.arange(0,360,5)
            cams = np.arange(0,66)
            idx = np.random.randint(cams.shape[0])
            cams = self.cam_rp_kinovis[idx]
            self.cams = np.insert(np.random.choice(cams, nb_cams, replace=False), 0, idx)

        elif self.cfg["data_type"] == "BlendedMVS":
            cams = self.scene_cam_clusters[subject]
            ref_camera = np.random.randint(len(cams))
            others = np.array(filter_blendedmvs_cluster(ref_camera, cams, self.all_calib[subject], nb_cams))
            self.cams = np.hstack((ref_camera, others))

            
        res = {}
        
        if self.cfg["gt_depthmaps_path"] is not None:
            depthmaps_gt, mask_depthmaps_gt = self.get_depthmaps(subject, self.cfg["gt_depthmaps_path"])
            tmp = {
                "depthmaps_gt": depthmaps_gt,
                "mask_depthmaps_gt": mask_depthmaps_gt,
            }
            res.update(tmp)

        tmp = self.get_images_calib(subject)
        res.update(tmp)
        samples_pos, samples_neg = self.compute_samples(subject, self.cfg["per_ray_samples"], self.height, self.width, depthmaps_gt, 
                self.calib, self.K, self.cam_centers, self.masks, tolerance=0.5, cams=[self.cams[0]])

        if self.cfg["window"] > 0:
            samples_gt = torch.zeros((samples_pos.shape[0] + samples_neg.shape[0]) // 21**2)
            samples_gt[:samples_gt.shape[0] // 2] = 1
        else:
            samples_gt = torch.zeros((samples_pos.shape[0] + samples_neg.shape[0]))
            samples_gt[:samples_pos.shape[0]] = 1
        
        tmp = {
            "samples_pos": samples_pos,
            "samples_neg": samples_neg,
            "samples_gt": samples_gt,
        }
        res.update(tmp)
        return res


    def __getitem__(self, index):
        return self.get_item(index)

