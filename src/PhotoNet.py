import pytorch_lightning as pl
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader

from PhotoDataset import *
from utils import *


class PhotoDataModule(pl.LightningDataModule):
    def __init__(self, cfg):
        self.cfg = cfg

    def setup(self, stage):
        self.dataset_tr = PhotoDataset(self.cfg, phase='train')
        self.dataset_val = PhotoDataset(self.cfg, phase='val')
        self.dataloader_tr = DataLoader(self.dataset_tr, batch_size=1, shuffle=True, num_workers=self.cfg["num_workers"], prefetch_factor=self.cfg["prefetch"])
        self.dataloader_val = DataLoader(self.dataset_val, batch_size=1, shuffle=False, num_workers=self.cfg["num_workers"])

    def train_dataloader(self):
        return self.dataloader_tr

    def val_dataloader(self):
        return self.dataloader_val



class PhotoNet(pl.LightningModule):
    def __init__(self, cfg={}):
        super().__init__()
        self.cfg = cfg
        if "num_rays" in self.cfg:
            self.num_rays = self.cfg["num_rays"]
        if "lr" in self.cfg:
            self.lr = self.cfg["lr"]

        if "window" in self.cfg:
            self.window = self.cfg["window"]
        else:
            self.window = 0

        if "data_type" in self.cfg:
            self.data_type = self.cfg["data_type"]
        else:
            self.data_type = "RP"
        
        
        self.maxpool = nn.MaxPool2d(2)
        self.actvn = nn.ReLU()
        self.final_act = nn.Sigmoid()

        self.photo_loss = nn.BCELoss()
        
        transfo_layer = 2
        transfo_ff_dim = 256

        if self.window == 0:
            # Convolution on the full image
            # ==============================
            self.conv_in = nn.Conv2d(in_channels=3, out_channels=16, kernel_size=3, stride=1, dilation=1, padding=1, padding_mode='zeros')
            self.conv_0 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, stride=1, dilation=1, padding=1, padding_mode='zeros')
            self.conv_0_1 = nn.Conv2d(in_channels=32, out_channels=32, kernel_size=3, stride=1, dilation=1, padding=1, padding_mode='zeros')
            self.conv_1 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, stride=1, dilation=1, padding=1, padding_mode='zeros')
            self.conv_1_1 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, stride=1, dilation=1, padding=1, padding_mode='zeros')
            self.feature_size = (3 + 16 + 32 + 64)
            self.conv_in_bn = nn.BatchNorm2d(16)
            self.conv0_1_bn = nn.BatchNorm2d(32)
            self.conv1_1_bn = nn.BatchNorm2d(64)
            self.fc_0 = nn.Linear(in_features=self.feature_size, out_features=256)
            self.fc_1 = nn.Linear(in_features=256, out_features=128)
            self.fc_out = nn.Linear(in_features=128, out_features=1)
            self.encoder_layer = nn.TransformerEncoderLayer(d_model=self.feature_size, nhead=1, dim_feedforward=transfo_ff_dim)
            self.transformer_encoder = nn.TransformerEncoder(self.encoder_layer, num_layers=transfo_layer)
            # ==============================
        else:
            # Convolution on neighbour points
            # ==============================
            self.conv0 = nn.Conv2d(in_channels=6, out_channels=32,  kernel_size=5, stride=1, dilation=1, padding=0, padding_mode='zeros')
            self.conv1 = nn.Conv2d(in_channels=32, out_channels=64,  kernel_size=5, stride=1, dilation=1, padding=0, padding_mode='zeros')
            self.max_pool = nn.MaxPool2d(2, stride=2)
            
            self.f0 = nn.Linear(64*2*2, 128)
            self.f1 = nn.Linear(128, 32)
            self.f2 = nn.Linear(32, 1)
            # ==============================

        self.transfo_fp32 = False
        
        self.save_hyperparameters()



    def forward(self, ref_images, ref_points):
        if self.window == 0:
            features_list = self.filter(ref_images=ref_images)
            out = self.predict(features_list=features_list, ref_pts=ref_points)
            out = out[0,:,0]
        else:
            ref_images = ref_images.permute(0,3,1,2)
            colors = F.grid_sample(ref_images, ref_points, align_corners=True)

            colors = colors.reshape(colors.shape[0], colors.shape[1], -1, self.window, self.window)
                    
            num_views = colors.shape[0]
            num_points = colors.shape[2]
            colors = colors.permute(0, 2, 1, 3, 4)
           
            colors_master = colors[0:1, :, :, :, :].repeat(num_views-1,1,1,1,1)
            colors_compared = colors[list(range(1, num_views)),:,:,:,:]
            colors = torch.cat([colors_master, colors_compared], axis=2)

            colors = colors.view(colors.shape[0]*colors.shape[1], colors.shape[2], colors.shape[3], colors.shape[4])

            out = self.actvn(self.conv0(colors))
            out = self.max_pool(out)
            out = self.actvn(self.conv1(out))
            out = self.max_pool(out)
            out = out.view(num_views-1, num_points, -1)
            out = out.mean(0, keepdim=True)
            out = out.permute(1,2,0)

            out = self.f0(out[:,:,0])
            out = self.actvn(out)
            out = self.f1(out)
            out = self.actvn(out)
            out = self.f2(out)
            out = self.final_act(out)
            out = out[:,0]
            
        return out



    def filter(self, ref_images):
        res = []
        ref_images = ref_images.permute((0,3,1,2))
        res.append(ref_images)
        net = self.actvn(self.conv_in(ref_images))
        net = self.conv_in_bn(net)
        res.append(net)
        if self.pool_1:
            net = self.maxpool(net)
        net = self.actvn(self.conv_0(net))
        net = self.actvn(self.conv_0_1(net))
        net = self.conv0_1_bn(net)
        res.append(net)
        if self.pool_2:
            net = self.maxpool(net)
        net = self.actvn(self.conv_1(net))
        net = self.actvn(self.conv_1_1(net))
        net = self.conv1_1_bn(net)
        res.append(net)
        return res


    def predict(self, features_list, ref_pts):
        num_views = ref_pts.shape[0]
        ref_pts_2 = ref_pts[:,:,0,:].permute(0,2,1)
        idx_a = ref_pts_2 > 1
        idx_b = ref_pts_2 < -1
        idx = torch.logical_or(idx_a, idx_b)
        idx = torch.logical_or(idx[...,0,:], idx[...,1,:]).T.int()
        idx = idx.reshape(-1, num_views)
        
        idx.unsqueeze_(2)
        idx = idx.repeat((1,1,num_views))
        idx2 = torch.transpose(idx, 1, 2)
        idx = torch.logical_or(idx, idx2).float()
        mask = idx.cuda()

        feature_0 = F.grid_sample(features_list[0], ref_pts, align_corners=True) # out (batch_size x num_ref_views, 3, rays, num_samples)
        feature_1 = F.grid_sample(features_list[1], ref_pts, align_corners=True) # out (batch_size x num_ref_views, 16, rays, num_samples)
        feature_2 = F.grid_sample(features_list[2], ref_pts, align_corners=True) # out (batch_size x num_ref_views, 32, rays, num_samples)
        feature_3 = F.grid_sample(features_list[3], ref_pts, align_corners=True) # out (batch_size x num_ref_views, 64, rays, num_samples)
        features = torch.cat((feature_0, feature_1, feature_2, feature_3), dim=1)  # out (batch_size x num_ref_views, features, rays, num_samples),

        # features: [num_cams, feat_size, 2*num_rays, 1]
        features = features.view(features.shape[0], features.shape[1],  features.shape[2]*features.shape[3])
        # features: [num_cams, feat_size, 2*num_rays]
        features_in = features.permute(0,2,1)
        # features: [num_cams, 2*num_rays, feat_size]
        
        if self.transfo_fp32:
            features = self.transformer_encoder(features_in.float())
        else:
            features = self.transformer_encoder(features_in)

        if self.transfo_fp32:
            features_avg = torch.mean(features.half(), dim=0, keepdim=True)
        else:
            features_avg = torch.mean(features, dim=0, keepdim=True)

        features_avg = self.actvn(self.fc_0(features_avg))
        features_avg = self.actvn(self.fc_1(features_avg))
        features_avg = self.fc_out(features_avg)
        features_avg = self.final_act(features_avg)
        return features_avg
    

    


    def training_step(self, batch, batch_idx):

        calib_tensor = batch["calib"][0]
        proj_tensor = batch["proj"][0]
        render_tensor = batch["render"][0]
        samples_pos = batch["samples_pos"][0]
        samples_neg = batch["samples_neg"][0]
        samples_gt = batch["samples_gt"][0]

        samples = torch.cat([samples_pos, samples_neg])

        ref_points = world_to_view_persp(samples.T, calib_tensor, proj_tensor, data_type=self.data_type)
        ref_points = ref_points.permute(0,2,1)
        ref_points = ref_points.view(ref_points.shape[0], ref_points.shape[1], 1, 4)
        ref_points = ref_points[:,:,:,:2]
        
        if self.data_type == "DTU" or self.data_type == "BlendedMVS":
            ref_points[:,:,:,0] = (ref_points[:,:,:,0] / (render_tensor.shape[2] / 2)) - 1
            ref_points[:,:,:,1] = (ref_points[:,:,:,1] / (render_tensor.shape[1] / 2)) - 1
        
        
        pred = self(render_tensor, ref_points)
        
        L = self.photo_loss(pred, samples_gt)
        
        acc, pre, rec, sp, sn = compute_metrics(pred, samples_gt)

        
        self.log_dict({
                'Loss/train': L,
                'Accuracy/train': acc,
                'Precision/train': pre,
                'Recall/train': rec,
                'Strict_pos/train': sp,
                'Strict_neg/train': sn,
            })
        return L




    def validation_step(self, batch, batch_idx):
        calib_tensor = batch["calib"][0]
        proj_tensor = batch["proj"][0]
        render_tensor = batch["render"][0]
        samples_pos = batch["samples_pos"][0]
        samples_neg = batch["samples_neg"][0]
        samples_gt = batch["samples_gt"][0]

        samples = torch.cat([samples_pos, samples_neg])
        
        ref_points = world_to_view_persp(samples.T, calib_tensor, proj_tensor, data_type=self.data_type)
        ref_points = ref_points.permute(0,2,1)
        ref_points = ref_points.view(ref_points.shape[0], ref_points.shape[1], 1, 4)
        ref_points = ref_points[:,:,:,:2]
        
        if self.data_type == "DTU" or self.data_type == "BlendedMVS":
            ref_points[:,:,:,0] = (ref_points[:,:,:,0] / (render_tensor.shape[2] / 2)) - 1
            ref_points[:,:,:,1] = (ref_points[:,:,:,1] / (render_tensor.shape[1] / 2)) - 1
        
        pred = self(render_tensor, ref_points)

        L = self.photo_loss(pred, samples_gt)

        acc, pre, rec, sp, sn = compute_metrics(pred, samples_gt)
        
        self.log_dict({
                'Loss/val': L,
                'Accuracy/val': acc,
                'Precision/val': pre,
                'Recall/val': rec,
                'Strict_pos/val': sp,
                'Strict_neg/val': sn,
            })


    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        return [optimizer], []