import argparse

import numpy as np
import open3d as o3d

from utils import *

parser = argparse.ArgumentParser(description='Compute normals')
parser.add_argument('-i', '--input', action='store', required=True, type=str, help='Input pointcloud')
args = parser.parse_args()


pcd = o3d.io.read_point_cloud(args.input)

# Filter outliers
radius = 5
pcd = pcd.voxel_down_sample(voxel_size=0.001)
pcd.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=10,max_nn=100))
pcd.orient_normals_consistent_tangent_plane(k=15)

# flip normals
if "scene_99" in args.input:
    normals = np.asarray(pcd.normals)
    pcd.normals = o3d.utility.Vector3dVector(-normals)


o3d.io.write_point_cloud(args.input.replace(".ply", "_nmls.ply"), pcd, write_ascii=False, compressed=False, print_progress=False)

